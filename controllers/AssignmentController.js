var Response = require('../utils/Response');
var DriverZoneAssignment = require('../models/DriverZoneAssignments');
var DriverDepartmentAssignment = require('../models/DriverDepartmentAssignments');
var mongoose = require('mongoose');

const ZoneController = {
    assign: function (req, res) {
        var data = req.body;
        DriverZoneAssignment.find({
            driver: data.driver,
            zone: data.zone
        })
            .then(assignment => {
                if (assignment.length !== 0)
                    return Response.badRequest(res, "Assignment already exist.");

                let zone = new DriverZoneAssignment(data);

                zone.save(function () {
                    Response.success(res, 'Assigned to zone successfully.', zone);
                });
            })

    },
    get: function (req, res) {
        DriverZoneAssignment.find()
            .populate('driver')
            .then(response => {
                Response.success(res, 'Successfully returned.', response);
            })
    },
    unAssign: function (req, res) {
        var assignId = req.params.assignId;
        DriverZoneAssignment.remove({_id: assignId})
            .then(response=>{
                Response.success(res, 'Sucessfully un-assigned.', response);
            })
    },
    assignToDepartment: function (req, res) {
        var data = req.body;
        var assign = new DriverDepartmentAssignment(data);
        assign.save(function (response) {
            Response.success(res, 'Assigned to department successfully.', assign);
        });
    },
    unAssignFromDepartment: function (req, res) {
        var assignId = req.params.assignId;
        DriverDepartmentAssignment.remove({_id: assignId})
            .then(response=>{
                Response.success(res, 'Sucessfully un-assigned.', response);
            })
    },
    getAssignedToDepartments: function (req, res) {
        DriverDepartmentAssignment.find()
            .populate('driver')
            .populate('company')
            .then(response=>{
                Response.success(res, 'Successfully returned.', response);
            })
    },
};

module.exports = ZoneController;