/**
 * Created by jeton on 6/14/2017.
 */
var AuthConfig = require('../config/auth0');
var passport = require('passport');

const AuthController = {
    login: function (req, res) {
        res.render('login', { env: AuthConfig.auth0});
    },
    saveSession: function (req, res) {
        passport.authenticate('auth0', { failureRedirect: '/login' }),
            function(req, res) {
                res.redirect(req.session.returnTo || '/dashboard');
            }
    }
};

module.exports = AuthController;