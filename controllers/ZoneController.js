/**
 * Created by jeton on 8/25/2017.
 */
/**
 * Created by jeton on 6/30/2017.
 */
var Response = require('../utils/Response');
var ZonesModel = require('../models/Zones');
var mongoose = require('mongoose');

const ZoneController = {
    get: function (req, res) {
        ZonesModel.find()
            .then(data => {
                if (data) {
                    let result = [];

                    data.forEach(zone => {
                        let current = {countryCode: zone["COUNTRY CODE"], area: zone.AREA, zone: zone.ZONE
                        , eveningDelivery:zone.EVENING||false, sameDay:zone.SAMEDAY||false};
                        let index = result.findIndex((item) => {
                            return ((item.area == current.area) && (item.zone == current.zone));
                        });
                        if (index === -1) {
                            current.postCodes = zone.ZIP;
                            result.push(current);
                        } else {
                            result[index].postCodes += ", " + zone.ZIP;
                        }
                    });
                    result.sort((a, b) => {
                        let firstArea = a.area + a.zone;
                        let secondArea = b.area + b.zone;
                        if (firstArea > secondArea) {
                            return 1;
                        }
                        if (firstArea < secondArea) {
                            return -1;
                        }
                        return 0;
                    });
                    Response.success(res, 'Successfully returned.', result);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    edit: function (req, res) {
        if (!req.body.zone || !req.body.oldZone) {
            Response.error(res, 'Missed required params');
            return false;
        }

        ZonesModel.update({'COUNTRY CODE': req.body.oldZone.countryCode, AREA: req.body.oldZone.area, ZONE: req.body.oldZone.zone}, {
            'COUNTRY CODE': req.body.zone.countryCode,
            AREA: req.body.zone.area,
            ZONE: req.body.zone.zone,
            EVENING: req.body.zone.eveningDelivery||false,
            SAMEDAY: req.body.zone.sameDay||false
        }, {multi: true})
            .then((err, zone) => {
                Response.success(res, 'Successfully updated the areas.');
            })
    },
    delete: function (req, res) {
        if (!req.query.area || !req.query.zone) {
            Response.error(res, 'Missed required params');
            return false;
        }

        ZonesModel.remove({AREA: req.query.area, ZONE: req.query.zone})
            .then((err, zone) => {
                Response.success(res, 'Successfully deleted the area.');
            })
    },
    getPostCodes: function (req, res) {
        if (!req.query.area || !req.query.zone) {
            Response.error(res, 'Missed required params');
            return false;
        }
        ZonesModel.find({AREA: req.query.area, ZONE: req.query.zone})
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    createPostCode: function (req, res) {
        if (!req.body.postCode) {
            Response.error(res, 'Missed required params');
            return false;
        }

        let postCode = new ZonesModel(req.body.postCode);

        postCode.save((err, record) => {
            if (err) {
                Response.error(res, err);
                return false;
            }
            Response.success(res, 'Successfully created the postCode.');
        });
    },
    editPostCode: function (req, res) {
        if (!req.body.postCode || !req.params.postCodeId) {
            Response.error(res, 'Missed required params');
            return false;
        }
        delete req.body.postCode._id;
        ZonesModel.update({_id: req.params.postCodeId}, {$set: req.body.postCode})
            .then((err, postCode) => {
                Response.success(res, 'Successfully updated the postCode.');
            })
    },
    deletePostCode: function (req, res) {
        if (!req.params.postCodeId) {
            Response.error(res, 'Missed required params');
            return false;
        }

        ZonesModel.remove({_id: req.params.postCodeId})
            .then((err, zone) => {
                Response.success(res, 'Successfully deleted the postCode.');
            })
    },
};

module.exports = ZoneController;