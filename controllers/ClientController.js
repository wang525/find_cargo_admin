/**
 * Created by jeton on 6/14/2017.
 */
var Response = require('../utils/Response');
var AccountModel = require('../models/Accounts');
var DeliverySettings = require('../models/DeliverySettings');
var ClientPayment = require('../models/ClientPayment');
var ClientExpense = require('../models/ClientExpense');
var Crypto = require('crypto');
var accountRepository = require("../repositories/account");
var departmentRepository = require("../repositories/department");
var bcrypt = require('bcryptjs');
var utilityService = require('../services/UtilityService');
var mongoose = require('mongoose');
var ApiUrl = require('../services/ApiUrl');
var request = require('request-promise');

const ClientController = {
    get: function (req, res) {
        AccountModel.find({buyer: "1"})
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    create: function (req, res) {
        let userData = req.body;
        if (!userData || !userData.email || !userData.password) {
            Response.error(res, 'No data');
            return;
        }

        accountRepository
            .getByEmail(userData.email)
            .then(function (account) {
                if (account) {
                    Response.error(res, 'Email is already taken');
                    return null;
                }

                return bcrypt.hash(userData.password, 10);
            })
            .then((hash) => {
                if (!hash) return null;
                userData.password = hash;
                userData.buyer = 1;
                userData.active = 1;
                userData.scheduledClient = "1";
                userData.name = '';
                userData.approved = false;
                userData.date = new Date();
                return utilityService.randomCrypto(16);
            })
            .then((apikey) => {
                if (!apikey) return null;
                userData.apikey = apikey;

                return accountRepository.createAccount(userData);
            })
            .then(function (account) {
                if (!account) return null;
                let department = departmentRepository.generateDefaultDepartment(account._id);
                return departmentRepository.createDepartment(department);
            })
            .then((result) => {
                if (!result) {
                    return;
                }

                // create default user settings
                return request({
                    headers: {
                        'Token': userData.apikey
                    },
                    uri: ApiUrl.getApiUrl(req.headers.host) + '/v1/settings/config/default',
                    method: 'POST'
                });
        })
        .then(result => {
            if (!result) {
                return;
            }

            return Response.success(res, 'Successfully created the client.');
        });
    },
    search: function (req, res) {
        var name = req.params.name;
        AccountModel.findOne({'name': new RegExp(name, 'i')})
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    edit: function (req, res) {
        delete req.body.client._id;
        AccountModel.update({_id: req.params.clientId}, {$set: req.body.client})
            .then((err, carrier) => {
                Response.success(res, 'Successfully updated the client.');
            })
    },
    changePassword: function (req, res) {
        delete req.body.client._id;
        delete req.body.client.confirmPassword;
        console.log(req.body);
        req.body.client.password = bcrypt.hashSync(req.body.client.password, 10);
        AccountModel.update({_id: req.params.clientId}, {$set: req.body.client})
            .then((err, carrier) => {
                Response.success(res, 'Successfully updated the client.');
            })
    },
    delete: function (req, res) {
        AccountModel.remove({_id: req.params.clientId})
            .then((err, carrier) => {
                Response.success(res, 'Successfully deleted the carrier.');
            })
    },
    getActive: function (req, res) {
        AccountModel.find({active: '1'})
            .then(data => {
                Response.success(res, 'Successfully returned.', data);
            });
    },
    schedule: function (req, res) {
        AccountModel.update({_id: req.params.clientId}, {$set: {scheduledClient: req.body.schedule}})
            .then((err, carrier) => {
                Response.success(res, 'Successfully scheduled the client.');
            })
    },
    activation: function (req, res) {
        AccountModel.update({_id: req.params.clientId}, {approved: req.body.approved})
            .then((err, client) => {
                Response.success(res, 'Successfully approved the client.');
            })
    },
    getScheduled: function (req, res) {
        AccountModel.find({scheduledClient: '1'})
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    scheduleSettings: function (req, res) {
        // AccountModel.update({_id: req.params.clientId, buyer: '1', scheduledClient: true}, {$set: {scheduledSettings: req.body.schedule}})
        //     .then((err, carrier)=>{
        //         Response.success(res, 'Successfully updated the schedule settings.');
        //     })
        delete req.body.schedule._id;
        delete req.body.schedule.__v;
        DeliverySettings.update({clientId: req.params.clientId}, {$set: req.body.schedule}, {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true
        })
            .then((carrier) => {
                Response.success(res, 'Successfully updated the schedule settings.');
            }, (err) => {
                Response.error(res, err.message)
            });

    },
    getScheduleSettings: function (req, res) {
        DeliverySettings.findOne({clientId: req.params.clientId})
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    setInsurance: function (req, res) {
        if (!req.params.clientId || (!req.body.hasOwnProperty("insurance"))) {
            Response.error(res, 'No data');
            return;
        }
        AccountModel.findOneAndUpdate({_id: req.params.clientId}, {$set: {insurance: req.body.insurance}})
            .then((client) => {
                Response.success(res, 'Successfully updated the client.', client);
            })
    },
    getPayments: function (req, res) {
        let clientId = req.params.clientId;

        return ClientPayment.find({ accountId: clientId.toObjectId() }).sort({ date: -1 }).then(data => {
            if (data) {
                Response.success(res, 'Successfully returned.', data);
            } else {
                Response.noData(res, 'Not found.');
            }
        });
    },
    createPayment: function (req, res) {
        let clientId = req.params.clientId;
        let clientPayment = new ClientPayment(req.body);
        clientPayment.accountId = clientId.toObjectId();
        clientPayment.date = new Date();

        return AccountModel.findOne({ _id: clientId.toObjectId() }).then(account => {
            if (!account) {
                return null;
            }

            return ClientPayment.create(clientPayment);
        }).then(data => {
            if (!data) {
                return Response.badRequest(res);
            }

            return Response.success(res, 'Successfully created.', data);
        });
    },
    updatePayment: function (req, res) {
        let clientId = req.params.clientId;
        let paymentId = req.params.paymentId;
        let clientPayment = req.body;

        return ClientPayment.findOneAndUpdate({ accountId: clientId.toObjectId(), _id: paymentId.toObjectId() }, { $set: clientPayment }, { new: true })
            .then(data => {
                if (!data) {
                    return Response.badRequest(res);
                }

                return Response.success(res, 'Successfully updated.', data);
            });
    },
    deletePayment: function (req, res) {
        let clientId = req.params.clientId;
        let paymentId = req.params.paymentId;

        return ClientPayment.remove({ accountId: clientId.toObjectId(), _id: paymentId.toObjectId() })
            .then(data => {
                if (!data) {
                    return Response.badRequest(res);
                }

                return Response.success(res, 'Successfully deleted.');
            });
    },
    getExpenses: function (req, res) {
        let clientId = req.params.clientId;

        return ClientExpense.find({ accountId: clientId.toObjectId() }).sort({ date: -1 })
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    createExpense: function (req, res) {
        let clientId = req.params.clientId;
        let clientExpense = new ClientExpense(req.body);
        clientExpense.accountId = clientId.toObjectId();
        clientExpense.date = new Date();

        return AccountModel.findOne({ _id: clientId.toObjectId() }).then(account => {
            if (!account) {
                return null;
            }

            return ClientExpense.create(clientExpense);
        }).then(data => {
            if (!data) {
                return Response.badRequest(res);
            }

            return Response.success(res, 'Successfully created.', data);
        });
    },
    updateExpense: function (req, res) {
        let clientId = req.params.clientId;
        let expenseId = req.params.expenseId;
        let clientExpense = req.body;

        return ClientExpense.findOneAndUpdate({ accountId: clientId.toObjectId(), _id: expenseId.toObjectId() }, { $set: clientExpense }, { new: true })
            .then(data => {
                if (!data) {
                    return Response.badRequest(res);
                }

                return Response.success(res, 'Successfully updated.', data);
            });
    },
    deleteExpense: function (req, res) {
        let clientId = req.params.clientId;
        let expenseId = req.params.expenseId;

        return ClientExpense.remove({ accountId: clientId.toObjectId(), _id: expenseId.toObjectId() })
            .then(data => {
                if (!data) {
                    return Response.badRequest(res);
                }

                return Response.success(res, 'Successfully deleted.');
            });
    }
};

module.exports = ClientController;