/**
 * Created by jeton on 8/25/2017.
 */
/**
 * Created by jeton on 6/30/2017.
 */
var Response = require('../utils/Response');
var DepartmentsModel = require('../models/Departments');
var mongoose = require('mongoose');

const DepartmentsController = {
    create: function (req, res) {
        var data = req.body;
        var zone = new DepartmentsModel();
        zone.save();
        Response.success(res, 'Department Successfully Created.', zone);
    },
    get: function (req, res) {
        DepartmentsModel.find()
            .then(response=>{
                Response.success(res, 'Successfully returned.', response);
            })
    }
};

module.exports = DepartmentsController;