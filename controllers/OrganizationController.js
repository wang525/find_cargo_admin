/**
 * Created by jeton on 6/30/2017.
 */
var Response = require('../utils/Response');
var AccountModel = require('../models/Accounts');
var mongoose = require('mongoose');

const OrganizationController = {
    get: function (req, res) {
        //NEED TO CHANGED AFTER LOGIN IMPLEMENTATION
        //var accountId = '59569c5040d8f7c82eeaf292';

        AccountModel.find({category: {$in: ['3', 'admin', 'sales']}})
            .then(data=>{
                if(data){
                    Response.success(res, 'Successfully returned.', data);
                }else{
                    Response.noData(res, 'Not found.');
                }
            });
    },
    getById: function (req, res) {
        var accountId = req.params.id;
        AccountModel.find({parent: mongoose.Types.ObjectId(accountId)})
            .then(data=>{
                if(data){
                    Response.success(res, 'Successfully returned.', data);
                }else{
                    Response.noData(res, 'Not found.');
                }
            });
    },
    editOrganization: function (req, res) {
        AccountModel.update({_id: req.params.id}, {$set: req.body})
            .then((err, carrier)=>{
                Response.success(res, 'Successfully updated the organization.');
            })
    },
    editAccount: function (req, res) {
        AccountModel.update({_id: req.params.id}, {$set: req.body})
            .then((err, carrier)=>{
                Response.success(res, 'Successfully updated the account.');
            })
    },
    delete: function (req, res) {
        AccountModel.remove({_id: req.params.id})
            .then((err, carrier)=>{
                Response.success(res, 'Successfully deleted.');
            })
    }
};

module.exports = OrganizationController;