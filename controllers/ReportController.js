let Response = require('../utils/Response');
let AdminActionsLogModel = require('../models/AdminActionsLog');
let mongoose = require('mongoose');
let moment = require('moment');

const ReportController = {
    getAdminActions: function (req, res) {
        let date = req.params.date;
        let startDate = moment.utc(date, "MM-DD-YYYY").startOf('day');
        let endDate = moment.utc(date, "MM-DD-YYYY").add(1, "days");

        return AdminActionsLogModel.find({
            date: {
                $gte: startDate.toDate(), $lt: endDate.toDate()
            }
        }).sort({ date: -1 }).then(response => {
            Response.success(res, 'Successfully returned.', response);
        })
    }
};

module.exports = ReportController;