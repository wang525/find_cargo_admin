/**
 * Created by jeton on 6/14/2017.
 */
var Response = require('../utils/Response');
var ScheduledDeliveryModel = require('../models/ScheduledDelivery');
var AccountModel = require('../models/Accounts');
var moment = require('moment');

const ClientController = {
    get_insurances: function (req, res) {
        let query = {
            deliverydate: {
                $lt: new Date(req.params.dateTo), $gte: new Date(req.params.dateFrom)
            },
            full_insurance: true,
            deleted:{$ne:true}
        };
        let deliveries = [];
        ScheduledDeliveryModel.find(query)
            .then(data => {
                if (data && data.length > 0) {
                    deliveries = data;
                    return AccountModel.find({
                        '_id': {$in: data.map(item => item.creator)}
                    }, {name: 1, approved: 1});
                } else {
                    return [];
                }
            })
            .then((accounts) => {
                deliveries = deliveries.filter(item => {
                    let client = accounts.find(elem => elem._id.equals(item.creator));
                    return client.approved;
                });
                
                if (accounts.length > 0 && deliveries.length > 0) {
                    let result = deliveries.map(item => {
                        let client = accounts.find(elem => elem._id.equals(item.creator));
                        return {
                            client: client.name,
                            date: item.deliverydate,
                            id: item.deliveryid,
                            value: item.full_insurance_value
                        }
                    });
                    Response.success(res, 'Successfully returned.', result);
                } else {
                    Response.noData(res, 'No data.');
                }
            });
    }
};

module.exports = ClientController;