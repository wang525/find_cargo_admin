/**
 * Created by jeton on 6/14/2017.
 */
var Response = require('../utils/Response');
var AccountModel = require('../models/Accounts');
let TrackingModel = require('../models/Tracking');
var mongoose = require('mongoose');
var utilityService = require('../services/UtilityService');
var accountRepository = require("../repositories/account");
var Mail = require('../services/Mail');
var bcrypt = require('bcryptjs');
var FleetModel = mongoose.models.truck;
if (!FleetModel) {
    FleetModel = require('../models/Fleet');
}

let moment = require('moment');

String.prototype.toObjectId = function () {
    var ObjectId = (require('mongoose').Types.ObjectId);
    return new ObjectId(this.toString());
};

const CarrierController = {
    get: function (req, res) {
        let filter = { carrier: "1" };
        if (req.query.active) {
            filter.active = "1";
            filter.activeCarrier = "1";
        }

        AccountModel.find(filter, '-pass')
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    create: function (req, res) {
        let userData = req.body;
        if (!userData || !userData.email || !userData.password) {
            Response.error(res, 'No data');
            return;
        }

        accountRepository
            .getByEmail(userData.email)
            .then(function (account) {
                if (account) {
                    Response.error(res, 'Email is already taken');
                    return null;
                }

                return bcrypt.hash(userData.password, 10);
            })
            .then((hash) => {
                if (!hash) return null;
                userData.password = hash;
                userData.carrier = 1;
                userData.active = 1;
                userData.ActiveCarrier = 1;
                userData.date = new Date();
                return utilityService.randomCrypto(16);
            })
            .then((apikey) => {
                if (!apikey) return null;
                userData.apikey = apikey;

                return accountRepository.createAccount(userData);
            })
            .then(function (account) {
                if (!account) return null;

                if (account.email !== '') {
                    Mail.setOptions(account.email, `Welcome to IDAG`, Mail.createCarrierMsg(account.email), () => {
                        Mail.send();
                    })
                }
                console.log('Fleet', FleetModel)
                var new_fleet = new FleetModel({
                    "identifier": "default vehicle",
                    "vehicalType": "Van",
                    "type": "2",
                    "allowCargo": "Machinery",
                    "userID": account._id
                });
                new_fleet.save(function (err, doc) {
                    console.log('err', doc, err)
                    Response.success(res, 'Successfully created the carrier.');
                })

            });
    },
    edit: function (req, res) {
        delete req.body.carrier._id;
        AccountModel.update({ _id: req.params.carrierId }, { $set: req.body.carrier })
            .then((err, carrier) => {
                Response.success(res, 'Successfully updated the carrier.');
            })
    },
    delete: function (req, res) {
        AccountModel.remove({ _id: req.params.carrierId })
            .then((err, carrier) => {
                Response.success(res, 'Successfully deleted the carrier.');
            })
    },
    getActive: function (req, res) {
        AccountModel.find({ activeCarrier: '1' }, '-pass')
            .then(data => {
                Response.success(res, 'Successfully returned.', data);
            });
    },
    getDeliveries: function (req, res) {
        AccountModel.find({ carrier: true })
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    getDriversByCarrierId: function (req, res) {
        var carrierId = req.params.carrierId;
        AccountModel.find({ carrier: '1', groupId: carrierId }, '-pass')
            .then(data => {
                if (data) {
                    Response.success(res, 'Successfully returned.', data);
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    assignCarrierToClient: function (req, res) {
        var clientId = req.params.clientId;
        var data = req.body;
        AccountModel.update({ _id: clientId }, { $push: { scheduledCarriers: data } })
            .then((err, carrier) => {
                Response.success(res, 'Successfully updated the client.');
            })
    },
    unAssignDriver: function (req, res) {
        var clientId = req.params.clientId;
        var driverId = req.params.driverId;
        AccountModel.update({ _id: clientId }, { $pull: { scheduledCarriers: { driver: driverId } } })
            .then((err, carrier) => {
                Response.success(res, 'Successfully updated the client.');
            })
    },
    getAssignedDrivers: function (req, res) {
        var drivers = [];
        var clientId = req.params.clientId;
        AccountModel.find({ _id: clientId }, 'scheduledCarriers')
            .then(data => {
                if (data) {
                    for (let assigned of (data[0].scheduledCarriers)) {
                        drivers.push(assigned.driver);
                    }
                    AccountModel.find({
                        '_id': { $in: drivers }
                    }).then(drivers => {
                        Response.success(res, 'Successfully returned.', drivers);
                    })
                } else {
                    Response.noData(res, 'Not found.');
                }
            });
    },
    getTrackingData: function (req, res) {
        var carrierId = req.params.carrierId;
        let date = req.params.date;

        
        AccountModel.findById(carrierId).then(carrier=>{
            if (carrier) {
                let result = {};
                result.carrier = carrier;

                
                let query = {
                    "carrier_id":carrierId.toObjectId()
                };
                if (date) {                    
                    let startDate = moment.utc(date, "MM-DD-YYYY").startOf('day');
                    let endDate = moment.utc(date, "MM-DD-YYYY").add(1, "days");                   
                    query['created_at'] = { $gte: startDate.toDate(), $lt: endDate.toDate()};                    
                }

                TrackingModel.aggregate([
                    { 
                        $match: query 
                    },
                    {
                        $sort:{created_at:-1}
                    },
                    {
                        $group: {
                            _id: '$truck_id',
                            data: {
                                $push : {                                
                                    latitude:"$latitude",
                                    longitude:"$longitude",   
                                    created_at:"$created_at"
                                }
                            }
                        },
                    },
                    {
                        $lookup:{
                            from:'truck',
                            localField:'_id',
                            foreignField:'_id',
                            as:'truck'
                        }
                    }
                ]).then(trackingData=>{
                    result.trackingData = trackingData;
                    Response.success(res, 'Successfully returned.', result);
                })
                .catch(err=>{
                    Response.error(res, err);
                })


                /*TrackingModel.find(query).then(trackingData=>{
                    console.log("trackingData", trackingData)
                    result.trackingData = trackingData;
                    Response.success(res, 'Successfully returned.', result);
                })
                .catch(err=>{
                    console.log('err', err)
                    Response.error(res, err);
                })*/

            }
            else {
                Response.noData(res, 'Not found.');
            }
        })
        .catch(err=>{
            Response.error(res, err);
        })
        


        
    },
};

module.exports = CarrierController;