/**
 * Created by jeton on 6/14/2017.
 */
var AuthConfig = require('../config/auth0');
var passport = require('passport');
var AccountModel = require('../models/Accounts');


const MainController = {
    dashboard: function (req, res) {
        //res.sendFile(path.join(__dirname + '/__index.html'));
        res.render('main', { user: req.user });
    }
};

module.exports = MainController;