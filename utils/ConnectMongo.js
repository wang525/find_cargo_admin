/**
 * Created by jeton on 6/20/2017.
 */
var env = process.env.NODE_ENV || "development";
var path = require("path");
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[env];
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const dbURI = `mongodb://${config.host}/${config.db}`;
mongoose.connect(dbURI);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbURI);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});