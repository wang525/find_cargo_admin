var https = require('https');
var mailgun = require('mailgun-js')({ apiKey: 'key-1d1455734118e45284625cc0aa24f3b9', domain: 'nemlevering.dk' });
var Mail = require('../services/Mail');
var Promise = require('bluebird');

var now = new Date();
var date = (now.getMonth() + 1) + '-' + now.getDate() + '-' + now.getFullYear();
var url = 'https://api.nemlevering.dk/v1/zone-delivery/pickups-date/' + date;

var request = function (url) {
    return new Promise((resolve, reject) => {
        https.get(url, (resp) => {
            var data = '';

            resp.on('data', (chunk) => {
                data += chunk;
            });

            resp.on('end', () => {
                resolve(JSON.parse(data));
            });
        }).on("error", (err) => {
            reject(err.message);
        });
    });
}

var getPickupWindow = function (item) {
    return item.pickup_deadline_to ? item.pickup_deadline + "-" + deadline.pickup_deadline_to : deadline.pickup_deadline;
}

var getMessage = function (items) {
    var message = '<html><head><style>table, th, td {border: 1px solid black;border-collapse: collapse;}th, td {padding: 15px;text-align: left;}</style></head><body>';
    message += '<table><tr><th>Client name</th><th>Address</th><th>Deadline from</th><th>Deadline to</th><th>Date</th><th>Week day</th><th># of deliveries</th></tr>';

    items.forEach(function (item) {
        var date = new Date(item.date);
        var dateStr = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`
        message += `<tr><td>${item.creator.name}</td><td>${item.pickupaddress + ' ' + item.pickupzip + ' ' + item.pickupcity}</td><td>${item.pickup_deadline}</td><td>${item.pickup_deadline_to}</td><td>${dateStr}</td><td>${item.deliverydayofweek}</td><td>${item.deliveriesCount}</td></tr>`
    });

    message += '</table></body></html>';
    console.log(message);
    return message;
}

var send = function (body) {
    Mail.setOptions('anibal@nemlevering.dk', 'Daily pickups report', body, function () { });
    Mail.send();
}

request(url)
    .then(function (data) {
        return getMessage(data);
    }).then(function (msg) {
        if (msg) {
            send(msg);
        }
    }).catch(function (error) {
        console.log("Error: " + error)
    });
