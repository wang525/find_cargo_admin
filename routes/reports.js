let express = require('express');
let router = express.Router();
let ReportController = require('../controllers/ReportController')

router.get('/reports/adminActions/:date', ReportController.getAdminActions);

module.exports = router;