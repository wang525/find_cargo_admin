/**
 * Created by jeton on 8/25/2017.
 */
var express = require('express');
var router = express.Router();
var ServiceController = require('../controllers/ServiceController');

router.get('/enabled-services', ServiceController.getEnabled);
router.get('/services', ServiceController.get);
router.post('/services', ServiceController.create);
router.put('/service/:serviceId', ServiceController.edit);
router.delete('/service/:serviceId', ServiceController.delete);

module.exports = router;