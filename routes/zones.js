/**
 * Created by jeton on 8/25/2017.
 */
var express = require('express');
var router = express.Router();
var ZoneController = require('../controllers/ZoneController');


router.get('/zones', ZoneController.get);
router.delete('/zones', ZoneController.delete);
router.put('/zone', ZoneController.edit);
router.get('/zone/postCodes', ZoneController.getPostCodes);
router.post('/zone/postCodes', ZoneController.createPostCode);
router.put('/zone/postCodes/:postCodeId', ZoneController.editPostCode);
router.delete('/zone/postCodes/:postCodeId', ZoneController.deletePostCode);


module.exports = router;