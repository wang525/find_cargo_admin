/**
 * Created by jeton on 8/25/2017.
 */
var express = require('express');
var router = express.Router();
var AssignmentController = require('../controllers/AssignmentController');
var DepartmentsController = require('../controllers/DepartmensController');


router.post('/assign/driver-zone', AssignmentController.assign);
router.get('/assigned/driver-zone', AssignmentController.get);
router.delete('/un-assign/:assignId', AssignmentController.unAssign);


router.post('/department', DepartmentsController.create);
router.get('/departments', DepartmentsController.get);
router.post('/assign/driver-department', AssignmentController.assignToDepartment);
router.delete('/un-assign/:assignId/driver-department', AssignmentController.unAssignFromDepartment);
router.get('/assigned/departments', AssignmentController.getAssignedToDepartments);


module.exports = router;