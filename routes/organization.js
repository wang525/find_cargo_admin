/**
 * Created by jeton on 7/7/2017.
 */
var express = require('express');
var router = express.Router();
var OrganizationController = require('../controllers/OrganizationController');


router.get('/organizations', OrganizationController.get);
router.get('/organization/:id', OrganizationController.getById);

router.put('/organization/:id', OrganizationController.editOrganization);
router.put('/account/:id', OrganizationController.editAccount);


router.delete('/organization/:id', OrganizationController.delete);
router.delete('/account/:id', OrganizationController.delete);

module.exports = router;