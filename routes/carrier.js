/**
 * Created by jeton on 7/7/2017.
 */
var express = require('express');
var router = express.Router();
var CarrierController = require('../controllers/CarrierController');


router.get('/carriers', CarrierController.get);
router.post('/carriers', CarrierController.create);
router.get('/carriers/active', CarrierController.getActive);
router.put('/carrier/:carrierId', CarrierController.edit);
router.delete('/carrier/:carrierId', CarrierController.delete);
router.get('/deliveries', CarrierController.getDeliveries);
router.get('/carrier/:carrierId/drivers', CarrierController.getDriversByCarrierId);
router.put('/client/:clientId/assign', CarrierController.assignCarrierToClient);
router.put('/client/:clientId/unassign/:driverId', CarrierController.unAssignDriver);
router.get('/client/:clientId/assigned/drivers', CarrierController.getAssignedDrivers);
router.get('/carrier/trackDetail/:carrierId/:date', CarrierController.getTrackingData);
//For test, enables for getting all
router.get('/carrier/trackDetail/:carrierId', CarrierController.getTrackingData);

module.exports = router;