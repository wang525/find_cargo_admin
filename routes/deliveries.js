var express = require('express');
var router = express.Router();
var DeliveryController = require('../controllers/DeliveryController');

router.get('/insurances/:dateFrom/:dateTo', DeliveryController.get_insurances);

module.exports = router;