/**
 * Created by jeton on 7/7/2017.
 */
var express = require('express');
var router = express.Router();
var ClientController = require('../controllers/ClientController');


router.get('/clients', ClientController.get);
router.post('/clients', ClientController.create);
router.get('/clients/search/:name', ClientController.search);
router.get('/clients/scheduled', ClientController.getScheduled);
router.put('/client/:clientId', ClientController.edit);
router.put('/client/change-password/:clientId', ClientController.changePassword);
router.put('/client/:clientId/schedule', ClientController.schedule);
router.put('/client/:clientId/activation', ClientController.activation);
router.put('/client/:clientId/schedule-settings', ClientController.scheduleSettings);
router.get('/client/:clientId/schedule-settings', ClientController.getScheduleSettings);
router.put('/client/:clientId/insurance', ClientController.setInsurance);
router.get('/clients/:clientId/payments', ClientController.getPayments);
router.post('/clients/:clientId/payments', ClientController.createPayment);
router.put('/clients/:clientId/payments/:paymentId', ClientController.updatePayment);
router.delete('/clients/:clientId/payments/:paymentId', ClientController.deletePayment);
router.get('/clients/:clientId/expenses', ClientController.getExpenses);
router.post('/clients/:clientId/expenses', ClientController.createExpense);
router.put('/clients/:clientId/expenses/:expenseId', ClientController.updateExpense);
router.delete('/clients/:clientId/expenses/:expenseId', ClientController.deleteExpense);
router.delete('/client/:clientId', ClientController.delete);

module.exports = router;