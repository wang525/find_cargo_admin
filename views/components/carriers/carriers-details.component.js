var module = angular.module("app");
module.component("carriersDetails", {
    templateUrl: "./components/carriers/carriers-details.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", "$scope", controller]
});

function controller($http, $stateParams, $state, $scope){
    this.carrierDetails = [];
    this.date = new Date();

    this.totalItems = 0;
    this.limit = 10;
    this.currentPage = 1;

    this.$onInit = () => {
        this.getCarrierDetails();
    };

    this.initializeMap = () => {
        let pointA = {};
        if (this.carrierDetails && this.carrierDetails.length > 0) {
            for (var i = 0; i < this.carrierDetails.length; i++) {
                if (this.carrierDetails[i].data.length > 0) {
                    pointA = this.carrierDetails[i].data[0];
                    break;
                }
            }
        }

        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;
        
        if (pointA) {
            map = new google.maps.Map(document.getElementById('routemap'), {
                zoom: 15,
                center: pointA
            });
    
            directionsDisplay.setMap(map);
            
            //for (var i = 1; i < 2/*this.carrierDetails.length*/; i++) {
            var truckPoints = [];
            if (this.carrierDetails[0] && this.carrierDetails[0].data.length > 0) {
                for (var j = 0; j < this.carrierDetails[0].data.length; j++) {
                    let truckPoint = {};
                    truckPoint['lng'] = this.carrierDetails[0].data[j].lng;
                    truckPoint['lat'] = this.carrierDetails[0].data[j].lat;
                    truckPoints.push(truckPoint);
                }        
                calculateAndDisplayRoute(directionsService, directionsDisplay, truckPoints);
            }
            //}
        }
    };
    
    function calculateAndDisplayRoute(directionsService, directionsDisplay, truckPoints) {
        var selectedMode = "DRIVING";
        var request = {};
        request['origin'] = truckPoints[0];
        request['destination'] = truckPoints[truckPoints.length - 1];
        request['travelMode'] = google.maps.TravelMode[selectedMode];
        if (truckPoints.length > 2) {
            let wayPoints = [];
            for (var i = 1; i < truckPoints.length - 1; i++) {
                let wayPoint = {};
                wayPoint['location'] = truckPoints[i];
                wayPoint['stopover'] = true;
                wayPoints.push(wayPoint);
            }
            request['waypoints'] = wayPoints;
        }
        
        directionsService.route(request, function(response, status) {
            if (status == 'OK') {
            directionsDisplay.setDirections(response);
            } else {
            //window.alert('Directions request failed due to ' + status);
            }
      });
    }

    this.getCarrierDetails = () => {
        let date = moment(this.date.toDateString()).format("MM-DD-YYYY");
        let rawData = {};
        let pageNum = 0;

        fetchCarrierDetails($http, $state.carriersId, date).then(details=>{
            rawData = details;
            if (rawData && rawData.carrier && rawData.trackingData.length > 0) {

                this.carrierDetails = [];
                for (var i = 0; i < rawData.trackingData.length; i++){
                    let truckData = {};
                    truckData['name'] = rawData.carrier.name;
                    truckData['vehicleType'] = rawData.trackingData[i].truck[0].vehicalType;

                    if (rawData.trackingData[i].data && rawData.trackingData[i].data.length > 0) {
                        let truckLocDatas = [];
                        for (var j = 0; j < rawData.trackingData[i].data.length; j++) {
                            let truckLocData = {};
                            truckLocData['lng'] = rawData.trackingData[i].data[j].longitude;
                            truckLocData['lat'] = rawData.trackingData[i].data[j].latitude;
                            truckLocData['date'] = rawData.trackingData[i].data[j].created_at;
                            truckLocDatas.push(truckLocData);
                            pageNum = pageNum + 1;
                        }
                        truckData['data'] = truckLocDatas;
                    }
                    this.carrierDetails.push(truckData);
                }
                console.log("carrier Details Data", this.carrierDetails)
            }
            
            this.totalItems = pageNum;
            this.initializeMap();
        })
    }
    this.getTrackingDetails = () => {
        this.getCarrierDetails();
    }
}

function fetchCarrierDetails($http, carriersId, date) {
    return $http.get(`${API_URL}/carrier/trackDetail/${carriersId}/${date}`)
        .then(function (response) {
            return response.data.body;
        })
}