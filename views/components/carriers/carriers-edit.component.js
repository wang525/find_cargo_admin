/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("carriersEdit", {
    templateUrl: "./components/carriers/carriers-edit.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams){
    this.ccodes = ccodes;
    this.carrier = $stateParams.carrier;

    this.updateCCODE = () => {
        var ccode = ccodes[this.carrier.codeISO];
        this.carrier.ccode = ccode;
    }
    this.countryOptions = [
        {"value": "45", "name": "(+45)"},
        {"value": "46", "name": "(+46)"},
    ];

    this.carrier.ccode = this.carrier.ccode||'45'

    this.edit = () => {
        this.carrier.codeISO = (this.carrier.ccode=='45'?'DK':(this.carrier.ccode=='46'?'SE':''))
        $http.put(`${API_URL}/carrier/${this.carrier._id}`, {carrier: this.carrier})
            .then(function (response) {
                $state.go('carriers');
            })
    }

}

function fetchList($http){
    return $http.get(`${API_URL}/carriers`)
        .then(function (response) {
            return response.data.body;
        })
}