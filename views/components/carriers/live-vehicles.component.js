/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("liveVehicles", {
    templateUrl: "../components/carriers/live-vehicles.component.html",
    controllerAs: "model",
    controller: ["$http", "socketService", controller]
});

function controller($http, socketService){
    
    
    this.deliveries = [];
    this.liveVehicles = [];
    this.truckMarkers = {};
    this.latLonVar = null;
    this.custom_autocomplete_pickup = new google.maps.places.Autocomplete(document.getElementById('custom_pickup_location_address'));
    this.googleMap = null;

    /* test data */
    // this.testData = [];

     /* init socket for live moving vehicles */
     this.initSocket = () => {
         console.log('initSocket')
        this.socket = socketService.connect();
    };

    /* init socket for live moving vehicles */
    this.closeSocket = () => {
        
        if (this.socket) {
            console.log('closeSocket')
           this.socket.emit('unlisten');
           this.socket.disconnect();
        }
       
   };

    this.$onInit = () => {
        let coord = [0, 0];
        this.getLiveVehicles(coord);

        /*Test code begin */
        // this.testData = [[77.556, 28.574], [77.656, 28.674], [77.756, 28.774]];
        /*Test code end */
        google.maps.event.addListener(this.custom_autocomplete_pickup, 'place_changed', () => {
            var place = this.custom_autocomplete_pickup.getPlace();
            if (!place) return;
            var address = place.address_components;
           
            if (place.geometry&&place.geometry.location) {
                var searchCoord = [];
                searchCoord[0] = place.geometry.location.lat();
                searchCoord[1] = place.geometry.location.lng();

                this.getLiveVehicles(searchCoord);
                // console.log('address', place.geometry.location.lat(), place.geometry.location.lng())
            }            
        });
        
    };

    this.createVehicleChannel=(truckId)=> {
        if (this.socket) {
            // console.log("truckId", truckId)
            if(truckId) {
                this.socket.on(`vehicle_location_changed_${truckId}`,  (data) => {
                    console.log("socket event data", data)
                    
                    let newPosition = new google.maps.LatLng(data.latitude, data.longitude);

                    if (this.truckMarkers[truckId]!=null) {
                        this.truckMarkers[truckId].setPosition(newTruckPos);
                    }
                    else if (this.googleMap) {
                        this.truckMarkers[truckId] = new google.maps.Marker({
                            position: newPosition,
                            icon:'img/truck.png',
                            map: this.googleMap,
                            label: {
                            text: 'truck',
                            color: "#ffffff",
                            fontSize: "13px",
                            }
                        });
                    }
                    
                });
            }
        }
    }

    this.getLiveVehicles = (coord) => {
        this.closeSocket();
        this.initSocket();
        fetchLiveVehicles($http, coord).then(vehicles=>{
            console.log("array ture", Array.isArray(vehicles))
            if (Array.isArray(vehicles)) {
                this.removeMarkers();
                this.truckMarkers = {};
                this.liveVehicles = vehicles;
                let currentPosArr = [];

                this.liveVehicles&&this.liveVehicles.map((vehicle, i)=>{
                    let currentPos = {};
                    
                    if (vehicle.liveDelivery&&vehicle.liveDelivery.location
                        &&vehicle.liveDelivery.location.coordinates&&vehicle.liveDelivery.location.coordinates.length==2) {

                        currentPos['lng'] = vehicle.liveDelivery.location.coordinates[0];
                        currentPos['lat'] = vehicle.liveDelivery.location.coordinates[1];
                        currentPosArr.push(currentPos);
                    }
                    this.createVehicleChannel(vehicle['_id']);
                })
                console.log("cureent pos arraya", currentPosArr)
                this.initializeMap(currentPosArr);
            }
            else {
                console.log("not array")
                this.removeMarkers();
                this.truckMarkers = {};
                this.liveVehicles = [];
            }
        })
        .catch(err=>{
            console.log("err", err)
            this.removeMarkers();
            this.truckMarkers = {};
            this.liveVehicles = [];
        })
    };

    /* remove Markers */
    this.removeMarkers = () => {
        console.log("remove markersss", this.truckMarkers)
        this.liveVehicles&&this.liveVehicles.map((vehicle, i)=>{
            
            if (vehicle.liveDelivery&&vehicle.liveDelivery.location
                &&vehicle.liveDelivery.location.coordinates&&vehicle.liveDelivery.location.coordinates.length==2) {

                this.truckMarkers[vehicle['_id']].setMap(null);
            }
        })
    }

    this.initializeMap = (curPosArr) => {

        console.log("google map")
        
        /* test code */
        let centerP = {};
        if (curPosArr && curPosArr.length > 0) {
            centerP = curPosArr[0];
        }
        
        map = new google.maps.Map(document.getElementById('routemap'), {
            zoom: 15,
            center: centerP
        });
        this.googleMap = map;

        let showLabel = "truck";
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < curPosArr.length; i++) {
            let truckPoint = {};
            truckPoint = curPosArr[i];
            bounds.extend(truckPoint);
            this.truckMarkers[this.liveVehicles[i]['_id']] = new google.maps.Marker({
                position: truckPoint,
                icon:'img/truck.png',
                map: map,
                label: {
                text: showLabel + ' ' + (i + 1),
                color: "#ffffff",
                fontSize: "13px",
                }
            });
        }
        map.fitBounds(bounds);

        // window.setInterval(this.moveMarker, 1000);
    };
    
    /* START for testing real time moving of the carriers */
    /*
    this.moveMarker = () => {
        for (var i = 0; i < 3; i++) {
            this.testData[i][0] += 0.01;
            this.testData[i][1] += 0.01;
            let newTruckPos = new google.maps.LatLng(this.testData[i][1], this.testData[i][0]);
            this.truckMarkers[this.liveVehicles[i]['_id']].setPosition(newTruckPos);
        }
    }
    */
    /* START for testing real time moving of the carriers */
}

function fetchLiveVehicles($http, coord){
    return $http.get(`${API_ENDPOINT_URL}v1/carriers/availabilityAll/${coord}`)
        .then(function (vehicles) {
            console.log("vehicles", vehicles)
            return vehicles.data;
        });
}