/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("carriers", {
    templateUrl: "../components/carriers/carriers.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams, showEntries){
    this.carriers = [];
    this.filteredCarriers = [];
    this.search = '';

    this.totalItems = 0;
    this.limit = 50;
    this.currentPage = 1;


    this.$onInit = () => {
        this.getCarrierList();
    };


    this.filterCarriers = function () {
        this.filteredCarriers = this.search ? this.carriers.filter((item) => {
            let found = false;
            if (item.name) {
                found = item.name.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1
            }

            if (!found && item.email) {
                found = item.email.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1
            }
            return found;
        }) : this.carriers.slice(0);
        this.totalItems = this.filteredCarriers.length;
    };

    this.setPage = function(page){
        this.currentPage = page;
    };

    this.getCarrierList = () => {
        fetchCarriers($http).then(carriers=>{
            console.log("carries", carriers)
            this.carriers = carriers;
            this.filteredCarriers = carriers.slice();
            this.totalItems = this.filteredCarriers.length;
        })
    };

    this.carrierEdit = (carrier) => {
        $state.go('carriersEdit', {carrier: carrier});
    };

    this.deleteCarrier = (carrier) => {
        $http.delete(`${API_URL}/carrier/${carrier._id}`)
            .then(response=>{
                if(response.data.success){
                    this.carriers.splice(this.carriers.indexOf(carrier), 1);
                    this.filteredCarriers.splice(this.filteredCarriers.indexOf(carrier), 1);
                    this.totalItems = this.filteredCarriers.length;
                }
            })
    };

    this.activateCarrier = (carrier) => {
        $http.put(`${API_URL}/carrier/${carrier._id}`, {carrier: carrier})
            .then(function (response) {

            })
    };
}

function fetchCarriers($http){
    return $http.get(`${API_URL}/carriers`)
        .then(function (response) {
            return response.data.body;
        })
}