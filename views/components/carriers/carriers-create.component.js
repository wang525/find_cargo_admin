var module = angular.module("app");
module.component("carriersCreate", {
    templateUrl: "./components/carriers/carriers-create.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.carrier = {};
    this.error = '';

    this.create = () => {
        $http.post(`${API_URL}/carriers`, this.carrier)
            .then((response) => {
                $state.go('carriers');
            }, (response) => {
                this.error = response.data.error;
            })
    }

}