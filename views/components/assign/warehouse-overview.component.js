var module = angular.module("app");

module.component("warehouseOverview", {
    templateUrl: "/components/assign/warehouse-overview.component.html",
    controllerAs: "model",
    controller: ["$http", controller]
});

function controller($http) {
    var ctrl = this;

    this.forDate = new Date();

    this.resultsList = [];

    this.planned = false;
    this.planningInProcess = false;
    this.assigned = false;
    this.workingDeliveries = []
    this.deliveryChecked = false

    this.loadDeliveriesForDate = () => {
        let date = moment(this.forDate.toDateString()).format('YYYY-MM-DD');
        this.assigned = false;
        this.planned = false;

        $http.get(`${API_ENDPOINT_URL}v1/route-planning/${date}`)
            .then((response) => {
                response.data.map((o)=>{
                    o.checked = false
                })
                ctrl.resultsList = response.data//.filter(o=>o.status==1);

                console.log('result', response.data.length, ctrl.resultsList.length)
            });
    };

    this.startPlanning = () => {
        this.planningInProcess = true;
        this.sendPlanningRequest()
            .then(() => {
                this.planningInProcess = false;
                this.planned = true;
            });
    };

    this.sendPlanningRequest = () => {
        this.workingDeliveries = this.resultsList.filter(o=>{
            // if(o.checked) {
            //     return {
            //         delivery_id : o.delivery_id
            //     }
            // }
            return o.checked
        })

        console.log('this.workingDeliveries', this.workingDeliveries)

        var param = this.workingDeliveries.map(o=>{
            //console.log('delivery', o)
            return {
                  delivery_id : o.delivery_id
             }
        });

        console.log(param)
        return $http.post(`${API_ENDPOINT_URL}v1/route-planning/warehouse`, {deliveries: param})
            .then(response => {
                let data = response.data;
                console.log('data', data)
                ctrl.resultsList = data.result;

                this.assigned = true;
                this.planned = true;
            })
    };

    this.toggleCheck = () => {
        this.resultsList.map(o=>{
            o.checked = this.deliveryChecked
        })
    }

    this.formatDuration = (secs) => {
        var mins = Math.round(secs/60)
        if(mins >= 60) {
            var hours = Math.floor(mins / 60)
            mins = mins % 60
        }

        return (hours>0? `${hours} hr ${mins} m` : `${mins}m`)
    }

    this.getAreas = (areas, seed) => {
        var filteredAreas = areas.filter(a => {
            return (1<<a.index)&seed
        })

        return filteredAreas.map((a)=>a.area).join(', ')
    }

    this.getDrops = (areas, seed) =>{
        var areaCodes = areas.filter(a => {
            return (1<<a.index)&seed
        }).map((a)=>a.area)
        

        return this.workingDeliveries.filter((o)=>{
            return areaCodes.indexOf(o.zone) != -1
        }).map((d)=>d.external_id).join(', ')
    }
}