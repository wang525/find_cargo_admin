/**
 * Created by jeton on 8/24/2017.
 */
var module = angular.module("app");
module.component("assignDriverZone", {
    templateUrl: "../components/assign/assign-driver-zone.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state){
    var me = this;
    this.client = {};
    this.carriers = [];
    this.drivers = [];
    this.selectedDriver = '';
    this.assignTo = '';
    this.assignedDriversToZones = [];

    this.zones = [];


    this.selectedZone = '';

    this.$onInit = () => {
        this.getData();
    };

    this.getData = () => {
        this.fetchCarriers($http).then(carriers=>{
            this.carriers = carriers;
        });
        fetchAssignedDriversToZones($http, this.client._id).then(assignedDrivers=>{
            me.assignedDriversToZones = assignedDrivers;
        });
        this.getZones();
    };

    this.selectCarrier = () => {
        fetchDriversByCarrierId($http, this.selectedCarrier._id)
            .then(drivers=>{
                this.drivers = drivers;
            })
    };

    this.getZones = (query) => {
        fetchZones($http).then(zones=>{
            this.zones = zones.map((item) => (item.area+item.zone));
        })
    };


    this.assign = () => {
        var assignment = {
            driver: this.selectedDriver,
            zone: this.selectedZone
        };

        if(this.selectedDriver == ''){
            assignment.driver = this.selectedCarrier._id;
        }
        $http.post(`${API_URL}/assign/driver-zone`, assignment)
            .then(function (response) {
                if(response.data.success){
                    toastr.success(response.data.msg);
                    fetchAssignedDriversToZones($http).then(assigned=>{
                        me.assignedDriversToZones = assigned;
                    });
                }
            })
            .catch(function (data) {
                toastr.error(data.data.msg);
            })
    };

    this.unAssign = (assigned) => {
        $http.delete(`${API_URL}/un-assign/${assigned._id}`)
            .then(function (response) {
                var deletedAssign = me.assignedDriversToZones.indexOf(assigned);
                me.assignedDriversToZones.splice(deletedAssign, 1);
                toastr.success(response.data.msg);
            })
    };

    this.unAssignDriver = (driver) => {
        $http.put(`${API_URL}/client/${this.client._id}/unassign/${driver._id}`)
            .then(function (response) {
                if(response.data.success){
                    var deletedAssign = me.assignedDrivers.indexOf(driver);
                    me.assignedDrivers.splice(deletedAssign, 1);
                }
            })
    };

    this.fetchCarriers = ($http) => {
        return $http.get(`${API_URL}/carriers`)
            .then(function (response) {
                return response.data.body;
            })
    };

    this.formatZoneLabel = (zone) => {
        return zone;
    };

    this.formatDriverLabel = (driver) => {
        if (driver&&driver.email) {
            //console.log("driver email", driver.email)
            return `${driver.email}, ${driver.name || 'Name not added'}`;
        }
        
    }
}

function fetchAssignedDriversToZones($http){
    return $http.get(`${API_URL}/assigned/driver-zone`)
        .then(function (response) {
            return response.data.body;
        })
}

function fetchZones($http){
    return $http.get(`${API_URL}/zones`)
        .then(function (response) {
            return response.data.body;
        })
}