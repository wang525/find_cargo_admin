var module = angular.module("app");

module.component("areaRouting", {
    templateUrl: "/components/assign/area-routing.component.html",
    controllerAs: "model",
    controller: ["$http", controller]
});

function controller($http) {
    var ctrl = this;

    this.forDate = new Date();

    this.resultsList = [];

    this.planned = false;
    this.planningInProcess = false;
    this.assigned = false;
    this.workingDeliveries = []
    this.deliveryChecked = false

    this.loadDeliveriesForDate = () => {
        let date = moment(this.forDate.toDateString()).format('YYYY-MM-DD');
        this.assigned = false;
        this.planned = false;

        $http.get(`${API_ENDPOINT_URL}v1/route-planning/${date}`)
            .then((response) => {
                response.data.map((o)=>{
                    o.checked = false
                })
                ctrl.resultsList = _.sortBy(response.data, (o)=>{
                    return o.zone
                })
                //.filter(o=>o.status==1);

                console.log('result', response.data.length, ctrl.resultsList.length)
            });
    };

    this.startPlanning = () => {
        this.planningInProcess = true;
        this.sendPlanningRequest((err, rets) => {
            console.log('plan result', err, rets)

            // var warehouses = []
            // _.each(rets, (zone)=>{
            //     _.each(zone.result, (w)=>{
            //         if(warehouses.indexOf(w.warehouse) == -1)
            //             warehouses.push(w.warehouse)
            //     })
            // })
            // this.warehouses = warehouses.map(w=>{
            //     var zones = []
            //     _.map(rets, (zone)=>{
            //         var z = _.find(zone.result, (o)=>{
            //             return o.warehouse == w
            //         })

            //         if(z) {
            //             zones.push({
            //                 name:zone.zone,
            //                 routes:z.routes
            //             })
            //         }
            //     })

            //     return {
            //         name:w,
            //         zones
            //     }
            // })

            //console.log('this.warehouses', this.warehouses)
            this.resultsList = rets
            this.assigned = true;
            this.planned = true;
            this.planningInProcess = false;
        });
    };

    this.sendPlanningRequest = (callback) => {
        this.workingDeliveries = this.resultsList.filter(o=>{
            // if(o.checked) {
            //     return {
            //         delivery_id : o.delivery_id
            //     }
            // }
            return o.checked
        })

        console.log('this.workingDeliveries', this.workingDeliveries)

        var zones = []
        this.workingDeliveries.map((d)=>{
            if(zones.indexOf(d.zone) == -1) zones.push(d.zone)
        })
        
        async.map(zones, (zone, cb)=>{
            var param = this.workingDeliveries.filter((o)=>o.zone == zone).map(o=>{
                //console.log('delivery', o)
                return {
                      delivery_id : o.delivery_id
                 }
            });
    
            return $http.post(`${API_ENDPOINT_URL}v1/route-planning/area`, {deliveries: param})
                .then(response => {
                    cb(null, {result:response.data.result, zone})
                })
        }, (err, rets)=>{
            callback(err, rets)
        })
    };

    this.toggleCheck = () => {
        this.resultsList.map(o=>{
            o.checked = this.deliveryChecked
        })
    }

    this.formatDuration = (secs) => {
        var mins = Math.round(secs/60)
        if(mins >= 60) {
            var hours = Math.floor(mins / 60)
            mins = mins % 60
        }

        return (hours>0? `${hours} hr ${mins} m` : `${mins}m`)
    }

    this.getAreas = (areas, seed) => {
        var filteredAreas = areas.filter(a => {
            return (1<<a.index)&seed
        })

        return filteredAreas.map((a)=>a.area).join(', ')
    }

    this.getDrops = (areas, seed) =>{
        var areaCodes = areas.filter(a => {
            return (1<<a.index)&seed
        }).map((a)=>a.area)
        

        return this.workingDeliveries.filter((o)=>{
            return areaCodes.indexOf(o.zone) != -1
        }).map((d)=>d.external_id).join(', ')
    }

    this.getDropsString = (deliveries)=>{
        return deliveries.join(", ")
    }
}