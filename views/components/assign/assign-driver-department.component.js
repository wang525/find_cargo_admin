/**
 * Created by jeton on 8/24/2017.
 */
var module = angular.module("app");
module.component("assignDriverDepartment", {
    templateUrl: "../components/assign/assign-driver-department.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state){
    var me = this;
    this.client = {};
    this.carriers = [];
    this.drivers = [];
    this.selectedDriver = '';
    this.assignTo = '';
    this.assignedDriversToDepartments = [];

    this.selectedCompany = {};
    this.selectedDepartment = {};

    this.$onInit = () => {
        this.getData();
    };

    this.getData = () => {
        fetchAssignedDriversToDepartments($http).then(assigned=>{
            me.assignedDriversToDepartments = assigned;
        });
        this.fetchCarriers($http).then(carriers=>{
            this.carriers = carriers;
        });
        this.getDepartments();
    };

    this.selectCarrier = () => {
        fetchDriversByCarrierId($http, this.selectedCarrier._id)
            .then(drivers=>{
                this.drivers = drivers;
            })
    };

    this.getDepartments = () => {
        fetchDepartments($http).then(departments=>{
            this.departments = departments;
        })
    };


    this.assign = () => {
        var assignment = {
            driver: this.selectedDriver,
            company: this.selectedCompany._id,
            department: this.selectedDepartment
        };

        $http.post(`${API_URL}/assign/driver-department`, assignment)
            .then(function (response) {
                if(response.data.success){
                    fetchAssignedDriversToDepartments($http).then(assigned=>{
                        me.assignedDriversToDepartments = assigned;
                        toastr.success(response.data.msg);
                    });
                }
            })
    };

    this.unAssign = (assigned) => {
        $http.delete(`${API_URL}/un-assign/${assigned._id}/driver-department`)
            .then(function (response) {
                var deletedAssign = me.assignedDriversToDepartments.indexOf(assigned);
                me.assignedDriversToDepartments.splice(deletedAssign, 1);

            })
    };


    this.fetchCarriers = ($http) => {
        return $http.get(`${API_URL}/carriers`)
            .then(function (response) {
                return response.data.body;
            })
    };

    this.formatDriverLabel = (driver) => {
        return `${driver.email}, ${driver.name || 'Name not added'}`;
    }
}

function fetchAssignedDriversToDepartments($http){
    return $http.get(`${API_URL}/assigned/departments`)
        .then(function (response) {
            return response.data.body;
        })
}

function fetchDepartments($http){
    return $http.get(`${API_URL}/departments`)
        .then(function (response) {
            return response.data.body;
        })
}