var module = angular.module("app");

module.component("routePlanning", {
    templateUrl: "/components/assign/route-planning.component.html",
    controllerAs: "model",
    controller: ["$http", controller]
});

function controller($http) {
    var ctrl = this;

    this.forDate = new Date();

    this.resultsList = [];

    this.planned = false;
    this.planningInProcess = false;
    this.assigned = false;

    this.loadDeliveriesForDate = () => {
        let date = moment(this.forDate.toDateString()).format('YYYY-MM-DD');
        this.assigned = false;
        this.planned = false;

        $http.get(`${API_ENDPOINT_URL}v1/route-planning/${date}`)
            .then((response) => {
                ctrl.resultsList = response.data;

                var param = ctrl.resultsList.map(o=>{
                    return {
                        delivery_id : o.delivery_id
                    }
                })
                console.log('param',param)
            });
    };

    this.startPlanning = () => {
        this.planningInProcess = true;
        this.sendPlanningRequest()
            .then(() => {
                this.planningInProcess = false;
                this.planned = true;
            });
    };

    this.sendPlanningRequest = () => {
        return $http.post(`${API_ENDPOINT_URL}v1/route-planning/zone`, {deliveries: this.resultsList})
            .then(response => {
                let data = response.data;
                ctrl.resultsList = data;

                this.assigned = true;
                this.planned = true;
            })
    };
}