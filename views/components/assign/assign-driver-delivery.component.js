var module = angular.module("app");

module.component("assignDriverDelivery", {
    templateUrl: "/components/assign/assign-driver-delivery.component.html",
    controllerAs: "model",
    controller: ["$http", "$timeout", "$location",  controller]
});

function controller($http, $timeout) {
    var ctrl = this;
    this.forFromDate = new Date();
    this.forToDate = new Date();
    this.forClientName = "";
    this.forRecipientName = "";
    this.forAddress = "";
    this.forArea = "";
    this.forZone = "";
    this.editingDeliveriesList = [];
    this.carriersList = [];
    this.carriersDataForSearch = [];
    this.notificationMessage = "";
    this.customNotificationMessage = "";
    this.notificatedDeliveries = [];
    this.notificationPatterns = [];
    this.notificationMessage = '';
    this.globalEditMode = false;
    this.currentlyEditing = {};
    this.editingEstimatedDate = "";
    this.newDeliveriesDate = {
        value: "",
        isValid: false
    };

    this.deliveryStatuses = [{
        name: 'Created',
        value: 1
    }, {
        name: 'In Progress',
        value: 2
    }, {
        name: 'Finished',
        value: 3
    }, {
        name: 'Not Received',
        value: 4
    }, {
        name: 'For re-delivery',
        value: 5
    }, {
        name: 'Returned',
        value: 6
    }];

    this.nemleveringAddNote = "";

    this.newDeliveriesStatus = this.deliveryStatuses[0];

    this.selectedCompany = null;
    this.filterStatus = '';
    this.statuses = [
        {id: 1, value: 'Created'},
        {id: 2, value: 'In Progress'},
        {id: 3, value: 'Finished'},
        {id: 4, value: 'Not Received'},
        {id: 5, value: 'For re-delivery'},
        {id: 6, value: 'Returned'},
    ];

    this.$onInit = function () {
        this.loadCarriers().then(function () {
            ctrl.loadDeliveriesForDate();
            ctrl.prepareCarriersDataForSearch();
            ctrl.getNotificationTemplates();
            ctrl.getDepartments();
        });
    };

    this.getDepartments = () => {
        fetchDepartments($http).then(departments=>{
            this.departments = departments;
        })
    };

    this.loadDeliveriesForDate = function () {
        //console.log("selectedCompany", this.selectedCompany)
        fetchDeliveriesForDay($http, this.getDate(this.forFromDate), this.getDate(this.forToDate))
            .then(function (deliveries) {
                ctrl.editingDeliveriesList = deliveries.map(x => {
                    x.editingEstimatedDate = x.estimated_delivery_time
                        ? moment(x.estimated_delivery_time, "HH:mm").toDate()
                        : null;
                    x.currentEditingAssigned = x.assigned_to;
                    x.selected = false;
                    x.strStatus = '' + x.status;
                    return x;
                });

                
                console.log("load delivery list==>", ctrl.editingDeliveriesList)
                ctrl.editingDeliveriesList = ctrl.editingDeliveriesList.filter(x =>
                    //x.creatorName.includes(ctrl.forClientName)
                    (!ctrl.selectedCompany || !ctrl.selectedCompany.companyName || (ctrl.selectedCompany.companyName == x.creatorName))
                    && x.recipientname.includes(ctrl.forRecipientName)
                    && ctrl.parseAddress(x).includes(ctrl.forAddress)
                    && (!x.area||x.area.includes(ctrl.forArea))
                    && (!x.zone||x.zone.toString().includes(ctrl.forZone))
                    && (!ctrl.filterStatus || (ctrl.filterStatus == x.status.toString()))
                );
            })
    };
    

    this.getDate = function (date) {
        return moment(date.toDateString()).format('MM-DD-YYYY');
    };

    this.loadCarriers = function () {
        return $http.get(`${API_URL}/carriers?active=1`)
            .then(function (response) {
                ctrl.carriersList = response.data.body;
            })
    };

    this.getNameOfCarrier = function (delivery) {
        return delivery.assigned_to ? this.findCarrierById(delivery.assigned_to).name : '-';
    };

    this.findCarrierById = function (id) {
        let pos = this.carriersList.map(function (carrier) {
            return carrier._id;
        }).indexOf(id);

        return this.carriersList[pos];
    };

    this.parseDateToString = function (date) {
        return date || "-";
    };

    this.parsePhone = function (phone) {
        return typeof phone === 'string' ? phone : phone.phone;
    };

    this.parseAddress = function (delivery) {
        return delivery.deliveryaddress + ", " + delivery.deliveryzip + " " + delivery.deliverycity;
    };

    this.prepareCarriersDataForSearch = function () {
        if (this.carriersDataForSearch.length !== 0)
            return;

        this.carriersDataForSearch = this.carriersList.map(function (carrier) {
            return {
                id: carrier._id,
                text: carrier.name
            };
        });
    };

    this.changeData = function (delivery) {
        this.editingDeliveriesList.find(result => result._id === delivery._id ).selected = true;
    };

    this.confirm = function () {
        var theAnswer = confirm("Do you want to send notification to user ?");

        if (theAnswer) {
            var sendNotifiation = true;
        }
        else {
            var sendNotifiation = false;
        }
        var data = this.editingDeliveriesList.map(x => {
            return {
                delivery_id: x._id,
                driver_id: x.currentEditingAssigned,
                order: x.orderIndex,
                estimated_delivery_time: x.editingEstimatedDate ? moment(x.editingEstimatedDate).format('HH:mm') : undefined,
                "deliverydate": x.deliverydate,
                "deliverywindowend": _tConvert(x.deliverywindowend + ':00'),
                "recipientemail": x.recipientemail,
                "recipientname": x.recipientname,
                "recipientphone": x.recipientphone.country_dial_code + x.recipientphone.phone,
                "clientName": x.creatorName,
                "selected": x.selected,
                "notificationSend": sendNotifiation
            };
        });
        console.log(data);
        this.updateList(data);
    }
    function _tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
    this.updateList = function (data) {
        $http.put(`${API_ENDPOINT_URL}v1/zone-delivery/list`, data)
            .then(function () {
                // nothing
            });
    };

    this.deleteAssignment = function () {
        this.editingDeliveriesList.filter(x => x.selected).map(x => x.currentEditingAssigned = undefined);

        // delivery.currentEditingAssigned = undefined;
    }

    this.updateDate = function () {
        var selectedDeliveryIds = this.editingDeliveriesList.filter(x => x.selected).map(x => x._id);
        
        var date = moment(this.newDeliveriesDate.value).format('YYYY-MM-DD');
        $http.post(`${API_ENDPOINT_URL}v1/zone-delivery/updatedate/` + date, selectedDeliveryIds)
            .then(function () {
                ctrl.loadDeliveriesForDate();
            });
    }

    this.updateStatus = function () {
        var selectedDeliveryIds = this.editingDeliveriesList.filter(x => x.selected).map(x => x._id);
        console.log("selected delivery id", selectedDeliveryIds)
        var status = this.newDeliveriesStatus.value;
        $http.post(`${API_ENDPOINT_URL}v1/zone-delivery/updatestatus/` + status, selectedDeliveryIds)
            .then(function () {
                ctrl.loadDeliveriesForDate();
            });
    }

    this.deleteDeliveries = function () {
        var selectedDeliveryIds = this.editingDeliveriesList.filter(x => x.selected).map(x => x._id);

        $http.post(`${API_ENDPOINT_URL}v1/zone-delivery/deleteMany`, selectedDeliveryIds)
            .then(function () {
                ctrl.loadDeliveriesForDate();
            });
    }

    this.resetNewDeliveriesDate = function () {
        this.newDeliveriesDate = {
            value: "",
            isValid: false
        };
    }

    this.resetnewDeliveriesStatus = function () {
        this.newDeliveriesStatus = this.deliveryStatuses[0];
    }

    this.checkIfNewDeliveriesDateValid = function () {
        this.newDeliveriesDate.isValid = moment.utc(this.newDeliveriesDate.value).isValid();
    }

    this.checkIfAnySelected = function () {
        return this.editingDeliveriesList.filter(x => x.selected).length > 0;
    }

    this.getSelectedDeliveries = function () {
        this.notificatedDeliveries = this.editingDeliveriesList.filter(x => x.selected).slice();
    }

    this.getNotificationTemplates = function () {
        return $http.get(`${API_ENDPOINT_URL}v1/scheduled/events/notification`)
            .then(function (response) {
                ctrl.notificationPatterns = response.data;
                //ctrl.notificationMessage = ctrl.notificationPatterns[0];
                ctrl.notificationMessage = 'Other';
            })
    }

    this.sendNotifications = function () {
        var selectedDeliveryIds = this.editingDeliveriesList.filter(x => x.selected).map(x => x._id);

        let message = this.notificationMessage !== 'Other' ? this.notificationMessage : this.customNotificationMessage;
        $http.post(`${API_ENDPOINT_URL}v1/scheduled/events/notification`, { notificationMessage: message, deliveryIds: selectedDeliveryIds, token: 'admintoken' })
            .then(res => {
                if (res && res.data) {
                    res.data.forEach(delivery => {
                        this.notificatedDeliveries.map(current => {
                            if (current._id === delivery.deliveryId) {
                                current.success = delivery.success;
                                current.message = delivery.message;
                                current.display = true;
                            }
                            return current;
                        });
                    });
                } else {
                    window.alert("Error occurred");
                }
                $timeout(() => {
                    this.notificatedDeliveries.map(delivery => {
                        delivery.display = false;
                        return delivery;
                    });
                }, 12000);
            }, (err) => {
                window.alert("Error occurred: " + err.data.error);
            })
    }

    this.AddNote = function () {
        var selectedDeliveryIds = this.editingDeliveriesList.filter(x => x.selected).map(x => x._id);

        var data = {};

        data['eventMessage'] = this.nemleveringAddNote; 
        if (selectedDeliveryIds && selectedDeliveryIds.length > 0) {
            for (var i = 0; i < selectedDeliveryIds.length; i++) {
                $http.post(`${API_ENDPOINT_URL}v1/scheduled/${selectedDeliveryIds[i]}/adminevents`, data)
                .then(res => {
                    this.nemleveringAddNote = "";
                }, (err) => {
                    console.log("catced error", err)
                    window.alert("Error occurred: " + err.data.error);
                })
            }
        }
    }

    this.editDelivery = function (selectedDeliveryId) {
    
        parent.location='/#!/assign-driver-delivery/' + selectedDeliveryId;
    }

    this.changeStatus = function(selectedDeliveryId, selectedStatus) {

        var status = parseInt(selectedStatus);
        var selectedIds = ['' + selectedDeliveryId]

        $http.post(`${API_ENDPOINT_URL}v1/zone-delivery/updatestatus/` + status, selectedIds)
            .then(function () {
                toastr.success(`Changed status successfully.`);
                ctrl.loadDeliveriesForDate();
            });
    }

    this.showStatus = function (idx) {
        if (idx == 1) {
            return "Created";
        }
        else if (idx == 2) {
            return "In Progress";
        }
        else if (idx == 3) {
            return "Finished"
        }
    }

    this.getLocalTime = (timestr) => {
        return moment(new Date(`2018-01-01T${timestr}:00.000Z`)).format("hh:mm A")
    }
}

function fetchDeliveriesForDay($http, fromDate, toDate) {
    //console.log("fromDate", fromDate)
    return $http.get(`${API_ENDPOINT_URL}v1/zone-delivery/datePeriod/${fromDate}/${toDate}`)
        .then(function (deliveries) {
            return deliveries.data;
        });
}

function fetchDepartments($http){
    return $http.get(`${API_URL}/departments`)
        .then(function (response) {
            return response.data.body;
        })
}