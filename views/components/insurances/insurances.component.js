var module = angular.module("app");
module.component("insurances", {
    templateUrl: "../components/insurances/insurances.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state) {
    this.deliveries = [];

    this.dateFrom = moment().subtract(1, 'months').toDate();
    this.dateTo = new Date();

    this.totalItems = 0;
    this.limit = 50;
    this.currentPage = 1;
    //

    this.$onInit = () => {
        this.getInsuranceDeliveries();
    };

    this.setPage = function (page) {
        this.currentPage = page;
    };

    this.getInsuranceDeliveries = () => {
        let dateFrom = moment(this.dateFrom.toDateString()).format("MM-DD-YYYY");
        let dateTo = moment(this.dateTo.toDateString()).format("MM-DD-YYYY");
        return $http.get(`${API_URL}/insurances/${dateFrom}/${dateTo}`)
            .then((response) => {

                this.deliveries = response.data.body ? response.data.body.map(item => {
                    item.date = moment(item.date).format("MM-DD-YYYY");
                    return item;
                }) : [];
            })
    };

}
