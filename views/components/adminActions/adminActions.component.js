var module = angular.module("app");
module.component("adminActions", {
    templateUrl: "../components/adminActions/adminActions.component.html",
    controllerAs: "model",
    controller: ["$http", "$stateParams", controller]
});

function controller($http, $stateParams) {
    self = this;
    this.date = new Date();
    this.logs = [];

    this.$onInit = () => {
        this.loadLogs();
    }

    this.loadLogs = () => {
        $http.get(`${API_URL}/reports/adminActions/${moment(this.date.toDateString()).format('MM-DD-YYYY')}`)
            .then(function (response) {
                self.logs = response.data.body;
            });
    }
}
