/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("organizationEdit", {
    templateUrl: "../components/organizations/organization-edit.component.html",
    controllerAs: "model",
    controller: ["$http", "$stateParams", controller]
});

function controller($http, $stateParams){
    this.ccodes = ccodes;
    this.organization = $stateParams.organization;
    this.showSuccess = false;
    this.showMsg = '';
    this.countryOptions = [
        {"value": "45", "name": "(+45)"},
        {"value": "46", "name": "(+46)"},
    ];

    this.organization.ccode = this.organization.ccode||'45'

    this.updateCCODE = () => {
        var ccode = ccodes[this.organization.codeISO];
        this.organization.ccode = ccode;
    }

    this.edit = (organization) => {
        this.organization.codeISO = (this.organization.ccode=='45'?'DK':(this.organization.ccode=='46'?'SE':''))
        $http.put(`${API_URL}/organization/${this.organization._id}`, this.organization)
            .then(response=>{
                this.showSuccess = response.data.success;
                this.showMsg = response.data.msg;
            })
    }
}
