/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("organizations", {
    templateUrl: "../components/organizations/organizations.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state){
    this.organizations = [];
    this.filteredOrganizations = [];
    this.search = '';

    this.totalItems = 0;
    this.limit = 50;
    this.currentPage = 1;

    this.$onInit = () => {
        this.getOrganizationsList();
    };

    this.filterOrganizations = function () {
        this.filteredOrganizations = this.search ? this.organizations.filter((item) => {
            let found = false;
            if (item.name) {
                found = item.name.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1
            }

            if (!found && item.email) {
                found = item.email.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1
            }
            return found;
        }) : this.organizations.slice(0);
        this.totalItems = this.filteredOrganizations.length;
    };

    this.getOrganizationsList = () => {
        fetchOrganizations($http).then(organizations=>{
            this.organizations = organizations
            this.filteredOrganizations = organizations.slice(0);
            this.totalItems = this.filteredOrganizations.length;
        })
    }
    this.setPage = function (page) {
        this.currentPage = page;
    };

    this.organizationEditView = (organization) => {
        $state.go('organizationEdit', {organization: organization})
    }

    this.delete = (organization) => {
        $http.delete(`${API_URL}/organization/${organization._id}`)
            .then(response=>{
                var deleted = this.organizations.indexOf(organization);
                this.organizations.splice(deleted, 1);
            })
    }

    this.changePassword = (organization) => {
        $state.go('clientsChangePassword', {client: organization});
    };
}

function fetchOrganizations($http) {
    return $http.get(`${API_URL}/organizations`)
        .then(function (response) {
            return response.data.body;
        })
}