/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("organizationDetails", {
    templateUrl: "../components/organizations/organizations-details.component.html",
    controllerAs: "model",
    controller: ["$http", "$stateParams", "$state", controller]
});

function controller($http, $stateParams, $state){
    this.coworkers = [];

    this.$onInit = () => {
        console.log("fet carrier ID", $stateParams)
        this.getCoWorkersList();
    };

    this.getCoWorkersList = () => {
        fetchCoworkers($http, $stateParams.organizationId).then(coworkers=>{
            this.coworkers = coworkers;
        })
    }

    this.delete = (coworker) => {
        $http.delete(`${API_URL}/account/${coworker._id}`)
            .then(response=>{
                var deleted = this.coworkers.indexOf(coworker);
                this.coworkers.splice(deleted, 1);
            })
    }

    this.coWorkerEditView = (coworker) => {
        $state.go('coworkerEdit', {coworker: coworker})
    }

}

function fetchCoworkers($http, organizationId) {
    return $http.get(`${API_URL}/organization/${organizationId}`)
        .then(function (response) {
            return response.data.body;
        })
}