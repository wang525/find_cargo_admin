/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("coworkerEdit", {
    templateUrl: "../components/organizations/coworker-edit.component.html",
    controllerAs: "model",
    controller: ["$http", "$stateParams", controller]
});

function controller($http, $stateParams){
    this.ccodes = ccodes;
    this.coworker = $stateParams.coworker;
    this.showSuccess = false;
    this.showMsg = '';

    this.updateCCODE = () => {
        var ccode = ccodes[this.coworker.codeISO];
        this.coworker.ccode = ccode;
    }

    this.edit = (coworker) => {
        $http.put(`${API_URL}/account/${this.coworker._id}`, this.coworker)
            .then(response=>{
                this.showSuccess = response.data.success;
                this.showMsg = response.data.msg;
            })
    }
}
