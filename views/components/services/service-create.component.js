var module = angular.module("app");
module.component("serviceCreate", {
    templateUrl: "./components/services/service-create.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.service = {};
    this.error = '';

    this.$onInit = () => {
        this.service['delivery_window_default'] = "16:00:00Z/21:00:00Z";
    }

    this.isValidJson = (json) => {
        try {
            JSON.parse(json);
            return true;
        } catch (e) {
            return false;
        }
    }

    this.create = () => {      
        console.log("service create", this.service)  
        if (this.service.delivery_window && !this.isValidJson(this.service.delivery_window)) {
            alert("Delivery Windows is not formated for JSON.")
            return 
        }

        if (this.service.delivery_area && !this.isValidJson(this.service.delivery_area)) {
            alert("Delivery Area is not formated for JSON.")
            return 
        }
        
        $http.post(`${API_URL}/services`, this.service)
            .then((response) => {
                $state.go('services');
            }, (response) => {
                this.error = response.data.error;
            })
    }

}