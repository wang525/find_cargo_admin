/**
 * Created by jeton on 8/24/2017.
 */
var module = angular.module("app");
module.component("services", {
    templateUrl: "../components/services/services.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state){
    var me = this;
    this.client = {};
    this.carriers = [];
    this.drivers = [];
    this.selectedDriver = '';
    this.assignTo = '';
    this.assignedDriversToZones = [];
    this.services = [];
    this.zones = [];


    this.selectedZone = '';

    this.$onInit = () => {
        this.getData();
    };

    this.getData = () => {
        this.fetchServices($http).then(services=>{
            //console.log('services', services)
            this.services = services;
        });
        
    };

    this.editView = (service) => {
        $state.go('serviceEdit', {service: service});
    };

    this.delete = (service) => {
        $http.delete(`${API_URL}/service/${service._id}`)
            .then(response => {
                if (response.data.success) {
                    this.services.splice(this.services.indexOf(service), 1);
                }
            });
    };


    this.fetchServices = ($http) => {
        return $http.get(`${API_URL}/services`)
            .then(function (response) {
                return response.data.body;
            })
    };

    
}
