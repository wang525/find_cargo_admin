var module = angular.module("app");
module.component("serviceEdit", {
    templateUrl: "./components/services/service-edit.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.error = '';
    this.service = $stateParams.service;
    
    this.isValidJson = (json) => {
        try {
            JSON.parse(json);
            return true;
        } catch (e) {
            return false;
        }
    }

    this.save = () => {
        console.log('save', this.service)
        
        if (this.service.delivery_window && !this.isValidJson(this.service.delivery_window)) {
            alert("Delivery Windows is not formated for JSON.")
            return 
        }

        if (this.service.delivery_area && !this.isValidJson(this.service.delivery_area)) {
            alert("Delivery Area is not formated for JSON.")
            return 
        }

        $http.put(`${API_URL}/service/${this.service._id}`, {service: this.service})
        .then((response) => {
            $state.go('services');
        }, (response) => {
            this.error = response.data.error;
        })
    }

}