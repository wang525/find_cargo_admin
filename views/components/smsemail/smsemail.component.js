var module = angular.module("app");
module.component("smsemail", {
    templateUrl: "../components/smsemail/smsemail.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state) {
    
    this.Items = [
        {
            "value" : "created",
            "name" : "Created"
        },
        {
            "value" : "planned",
            "name" : "Assigned"
        },
        {
            "value" : "completed",
            "name" : "15 minutes before arrival"
        },
        {
            "value" : "on-way",
            "name" : "Finished"
        },
        {
            "value" : "delay",
            "name" : "Delayed"
        }
    ]

    this.test = "test";
    this.selectedEmail = '';
    this.selectedSMS = '';
    this.SMSContent = '';
    this.EmailContent = '';

    this.$onInit = () => {
        
    };
 
    this.getSMSContent = () => {
        fetchSMSContent($http, this.selectedSMS.value).then(details=>{
            if (details != null) {
                this.SMSContent = details.content;
            }
            else {
                this.SMSContent = ''
            }
        });
    }

    this.getEmailContent = () => {
        fetchEmailContent($http, this.selectedEmail.value).then(details=>{
            if (details != null) {
                this.EmailContent = details.content;
            }
            else {
                this.EmailContent = ''
            }
        });
    }

    this.SaveContent = () => {
        let promises = [];
        let promise1 = new Promise((resolve, reject)=>{
            if (this.selectedSMS != '') {
                let postSMS = {};
                postSMS['type'] = this.selectedSMS.value;
                postSMS['content'] = this.SMSContent;
                
                $http.post(`${API_ENDPOINT_URL}v1/template/sms`, postSMS).then(function (response) {
                    if (response.data.success === true) {
                        //alert("The content of SMS is successfully saved.")
                        resolve(true)
                    }
                    else {
                        reject(new Error('Failed update'));
                    }
                });
            }
            else {
                resolve(false);
            }
        })

        let promise2 = new Promise((resolve, reject)=>{
            if (this.selectedEmail != '') {
                let postEmail = {};
                postEmail['type'] = this.selectedEmail.value;
                postEmail['content'] = this.EmailContent;

                $http.post(`${API_ENDPOINT_URL}v1/template/email`, postEmail).then(function (response) {
                    if (response.data.success === true) {
                        //alert("The content of SMS is successfully saved.")
                        resolve(true)
                    }
                    else {
                        reject(new Error('Failed update'));
                    }
                });
            }
            else {
                resolve(false);
            }
        })

        promises.push(promise1);
        promises.push(promise2);
        Promise.all(promises).then(results=>{
            alert("The content is successfully saved.");
        })
        .catch(err=>{
            alert(err.message);
        })
    }
}

function fetchSMSContent($http, selectedSMSValue) {
    return $http.get(`${API_ENDPOINT_URL}v1/template/sms/admin/${selectedSMSValue}`)
        .then(function (response) {
            console.log("response sms", response)
            return response.data.data;
        })
}

function fetchEmailContent($http, selectedEmailValue) {
    return $http.get(`${API_ENDPOINT_URL}v1/template/email/admin/${selectedEmailValue}`)
        .then(function (response) {
            console.log("response email", response)
            return response.data.data;
        })
}
