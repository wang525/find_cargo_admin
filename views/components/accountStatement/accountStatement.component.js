var module = angular.module("app");
module.component("accountStatement", {
    templateUrl: "../components/accountStatement/accountStatement.component.html",
    controllerAs: "model",
    controller: ["$http", "$stateParams", controller]
});

function controller($http, $stateParams) {
    that = this;
    this.client = $stateParams.client;
    this.payments = [];
    this.expenses = [];
    this.newPayment = {
        description: '',
        value: 0
    }
    this.newExpense = {
        description: '',
        value: 0,
        paid: false
    }
    this.selectedPayment = null;
    this.selectedExpense = null;

    this.$onInit = () => {
        this.getPayments();
        this.getExpenses();
    }

    this.setCurrentPayment = (payment) => {
        this.selectedPayment = payment;
    }

    this.setCurrentExpense = (expense) => {
        this.selectedExpense = expense;
    }

    this.addPayment = () => {
        $http.post(`${API_URL}/clients/${this.client._id}/payments`, this.newPayment)
            .then(function (response) {
                that.newPayment = {
                    description: '',
                    value: 0
                }

                that.getPayments();
            });
    }

    this.addExpense = () => {
        $http.post(`${API_URL}/clients/${this.client._id}/expenses`, this.newExpense)
            .then(function (response) {
                that.newExpense = {
                    description: '',
                    value: 0,
                    paid: false
                }

                that.getExpenses();
            });
    }

    this.editPayment = () => {
        let editedPayment = {
            description: this.selectedPayment.description,
            value: this.selectedPayment.value
        };

        $http.put(`${API_URL}/clients/${this.client._id}/payments/${this.selectedPayment._id}`, editedPayment)
            .then(function (response) {
                that.getPayments();
            });
    }

    this.editExpense = () => {
        let editedExpense = {
            description: this.selectedExpense.description,
            value: this.selectedExpense.value,
            paid: this.selectedExpense.paid
        };

        $http.put(`${API_URL}/clients/${this.client._id}/expenses/${this.selectedExpense._id}`, editedExpense)
            .then(function (response) {
                that.getExpenses();
            });
    }

    this.deletePayment = (id) => {
        $http.delete(`${API_URL}/clients/${this.client._id}/payments/${id}`)
            .then(function (response) {
                that.getPayments();
            });
    }

    this.deleteExpense = (id) => {
        $http.delete(`${API_URL}/clients/${this.client._id}/expenses/${id}`)
            .then(function (response) {
                that.getExpenses();
            });
    }

    this.getPayments = () => {
        $http.get(`${API_URL}/clients/${this.client._id}/payments`)
            .then(function (response) {
                that.payments = response.data.body;
            });
    }

    this.getExpenses = () => {
        $http.get(`${API_URL}/clients/${this.client._id}/expenses`)
            .then(function (response) {
                that.expenses = response.data.body;
            });
    }

    this.checkIfNumber = (value) => {
        return value > 0 && !isNaN(parseFloat(value)) && isFinite(value);
    }
}
