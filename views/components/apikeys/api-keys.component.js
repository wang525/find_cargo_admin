/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("apiKeys", {
    templateUrl: "../components/apikeys/api-keys.component.html",
    controllerAs: "model",
    controller: ["$http", "$stateParams", controller]
});

function controller($http, $stateParams){
    var me = this;
    this.client = $stateParams.client;

    this.$onInit = () => {
        me.getApiKeys();
    };

    this.getApiKeys = () => {
        fetchApiKeys($http, me.client).then(client => {
            if (client) {
                this.client = client;
            }
        })
    };

    this.regenerate = () => {
        if (confirm('Are you sure you want to re-generate API-KEY?')) {
            $http.put(`${API_ENDPOINT_URL}v1/apikeys/${this.client._id}`)
                .then(response => {
                    if (response) {
                        me.client = response.data;
                    }
                })
        }
    };

    this.setInsurance = () => {
            $http.put(`${API_URL}/client/${this.client._id}/insurance`, {insurance: this.client.insurance})
                .then(function (response) {})
    };


    this.activateCarrier = (carrier) => {
        $http.put(`${API_URL}/carrier/${carrier._id}`, {carrier: carrier})
            .then(function (response) {})
    };
}

function fetchApiKeys($http, client){
    return $http.get(`${API_ENDPOINT_URL}v1/apikeys/${client._id}`)
        .then(function (response) {
            return response.data;
        })
};