/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("login", {
    templateUrl: "./login.component.html",
    controllerAs: "model",
    controller: ["$http", "authService", controller]
});



function controller($http, authService){
    var vm = this;
    vm.auth = authService;
}
