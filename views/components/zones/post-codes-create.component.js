var module = angular.module("app");
module.component("postCodesCreate", {
    templateUrl: "./components/zones/post-codes-create.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.postCode = {};
    this.error = '';

    this.$onInit = () => {
        this.zone = $stateParams.zone;
        if (!this.zone) return false;

        this.postCode.AREA = this.zone.area;
        this.postCode.ZONE = this.zone.zone;
        this.postCode.SAMEDAY = this.zone.sameDay;
        this.postCode.EVENING = this.zone.eveningDelivery;
    };

    this.create = () => {
        $http.post(`${API_URL}/zone/postCodes`, {postCode: this.postCode})
            .then((response) => {
                $state.go('postCodes', {zone: this.zone});
            }, (response) => {
                this.error = response.data.error;
            })
    }

}