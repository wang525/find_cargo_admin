var module = angular.module("app");
module.component("zonesEdit", {
    templateUrl: "./components/zones/zones-form.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {

    this.$onInit = () => {
        this.oldZone = Object.assign({}, $stateParams.zone);
        this.zone = $stateParams.zone;
    };

    this.save = () => {
        $http.put(`${API_URL}/zone`, {oldZone: this.oldZone, zone: this.zone})
            .then(function (response) {
                $state.go('zones');
            })
    }

}