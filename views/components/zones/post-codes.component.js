var module = angular.module("app");
module.component("postCodes", {
    templateUrl: "../components/zones/post-codes.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.postCodes = [];
    this.filteredPostCodes = [];
    this.search = '';

    this.totalItems = 0;
    this.limit = 50;
    this.currentPage = 1;


    this.$onInit = () => {
        this.zone = $stateParams.zone;
        this.isNew = $stateParams.isNew;
        if (!this.zone || this.isNew) return false;

        this.getPostCodes();
    };

    this.filterPostCodes = function () {
        this.filteredPostCodes = this.postCodes.filter((item) => {
            return item.ZIP.toString().indexOf(this.search) > -1;
        });
        this.totalItems = this.filteredPostCodes.length;
    };

    this.setPage = function (page) {
        this.currentPage = page;
    };

    this.getPostCodes = () => {
        fetchPostCodes(this.zone, $http).then(postCodes => {
            this.postCodes = postCodes;
            this.filteredPostCodes = postCodes.slice();
            this.totalItems = this.filteredPostCodes.length;
        })
    };
    //
    this.create = () => {
        $state.go('postCodesCreate', {zone: this.zone});
    };

    this.editPostCode = (postCode) => {
        $state.go('postCodesEdit', {postCode: postCode});
    };

    this.delete = (postCode) => {
        if (confirm("Are you Sure?")) {
            $http.delete(`${API_URL}/zone/postCodes/${postCode._id}`)
                .then(response => {
                    if (response.data.success) {
                        this.postCodes.splice(this.postCodes.indexOf(postCode), 1);
                        this.filteredPostCodes.splice(this.filteredPostCodes.indexOf(postCode), 1);
                        this.totalItems = this.filteredPostCodes.length;
                    }
                });
        }
    };
}

function fetchPostCodes(zone, $http) {
    return $http.get(`${API_URL}/zone/postCodes?area=${zone.area}&zone=${zone.zone}`)
        .then(function (response) {
            return response.data.body;
        })
}
