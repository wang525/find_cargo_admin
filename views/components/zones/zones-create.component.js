var module = angular.module("app");
module.component("zonesCreate", {
    templateUrl: "./components/zones/zones-form.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.zone = {};

    this.save = () => {
        $state.go('postCodes', {zone: this.zone, isNew: 1});
    }

}