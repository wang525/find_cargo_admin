var module = angular.module("app");
module.component("postCodesEdit", {
    templateUrl: "./components/zones/post-codes-edit.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.error = '';

    this.$onInit = () => {
        this.postCode = $stateParams.postCode;
    };

    this.edit = () => {
        $http.put(`${API_URL}/zone/postCodes/${this.postCode._id}`, {postCode: this.postCode})
            .then((response) => {
                $state.go('postCodes', {zone: {area: this.postCode.AREA, zone: this.postCode.ZONE}});
            }, (response) => {
                this.error = response.data.error;
            })
    }

}