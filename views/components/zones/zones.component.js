var module = angular.module("app");
module.component("zones", {
    templateUrl: "../components/zones/zones.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state) {
    this.nextZones = [];
    this.sameZones = [];
    this.filteredNextZones = [];
    this.filteredSameZones = [];
    this.search = '';

    this.totalSameItems = 0;
    this.totalNextItems = 0;
    this.limit = 50;
    // this.currentPage = 1;
    this.currentSamePage = 1;
    this.currentNextPage = 1;

    this.$onInit = () => {
        this.getZones();
    };

    // this.filterZones = function () {
    //     this.filteredZones = this.zones.filter((item) => {
    //         return (item.area + item.zone).indexOf(this.search) > -1;
    //     });
    //     this.totalItems = this.filteredZones.length;
    // };

    this.filterNextZones = function () {
        this.filteredNextZones = this.nextZones.filter((item) => {
            return (item.area + item.zone).indexOf(this.search) > -1;
        });
        this.totalNextItems = this.filteredNextZones.length;
    }

    this.filterSameZones = function () {
        this.filteredSameZones = this.sameZones.filter((item) => {
            return (item.area + item.zone).indexOf(this.search) > -1;
        });
        this.totalSameItems = this.filteredSameZones.length;
    }

    this.setPage = function (page) {
        this.currentPage = page;
    };

    this.getZones = () => {
        this.totalNextItems = 0;
        this.totalSameItems = 0;
        this.nextZones = [];
        this.sameZones = [];
        this.filteredNextZones = [];
        this.filteredSameZones = [];

        fetchZones($http).then(zones => {
            if (zones && zones.length > 0) {
                for (var i = 0; i < zones.length; i++) {
                    if (zones[i].sameDay == true) {
                        let sameZone = zones[i];
                        this.sameZones.push(sameZone);
                        this.totalSameItems += 1;
                    }
                    else {
                        let nextZone = zones[i];
                        this.nextZones.push(nextZone);
                        this.totalNextItems += 1;
                    }
                }
            }
            // this.zones = zones;
            // this.totalItems = this.filteredZones.length;
            this.filteredNextZones = this.nextZones.slice();
            this.filteredSameZones = this.sameZones.slice();
            console.log("next zone", this.nextZones);
            console.log("same zone", this.sameZones);
        })
    };

    this.editView = (zone) => {
        $state.go('zonesEdit', { zone: zone });
    };

    this.viewPostCodes = (zone) => {
        $state.go('postCodes', { zone: zone });
    };

    this.delete = (zone) => {
        if (confirm("Are you Sure? All post codes related to this zone will be removed.")) {
            $http.delete(`${API_URL}/zones?area=${zone.area}&zone=${zone.zone}`)
                .then(response => {
                    if (response.data.success) {
                        this.zones.splice(this.zones.indexOf(zone), 1);
                        this.filteredZones.splice(this.filteredZones.indexOf(zone), 1);
                        this.totalItems = this.filteredZones.length;
                    }
                });
        }
    };
}

function fetchZones($http) {
    console.log("fetch zone")
    return $http.get(`${API_URL}/zones`)
        .then(function (response) {
            console.log("zone response", response)
            return response.data.body;
        })
}
