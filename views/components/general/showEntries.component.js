/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("showEntries", {
    templateUrl: "../components/general/showEntries.component.html",
    controller: ["$http", "$state", controller],
    bindings: {
        limitTo: '='
    }
});

function controller($http, $state) {
    var vm = this;

    vm.limits = [
        10, 25, 50, 100
    ];
    vm.limit = 50;
}