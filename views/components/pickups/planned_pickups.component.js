var module = angular.module("app");
module.component("plannedPickups", {
    templateUrl: "../components/pickups/planned_pickups.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state) {
    this.pickups = [];
    this.filteredPickups = [];
    this.search = '';

    this.totalItems = 0;
    this.limit = 50;
    this.currentPage = 1;

    this.$onInit = () => {
        $http.get(`${API_ENDPOINT_URL}v1/zone-delivery/pickups-date`)
            .then((pickups) => {
                if (pickups.data && pickups.data.length > 0) {
                    this.pickups = pickups.data.map(item => {
                        item.deliverydate = moment(item.deliverydate, moment.ISO_8601).format("L");
                        return item;
                    });
                    this.filteredPickups = this.pickups.slice();
                    this.totalItems = this.filteredPickups.length;
                }
            })
    };


    this.setPage = function (page) {
        this.currentPage = page;
    };

    this.filterPickups = function () {
        this.filteredPickups = this.pickups.filter((item) => {
            return item.creator.name ? item.creator.name.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1 : false;
        });
        this.totalItems = this.filteredPickups.length;
    };

}