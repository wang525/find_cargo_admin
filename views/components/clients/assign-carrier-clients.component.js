/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("assignCarrierClient", {
    templateUrl: "../components/clients/assign-carrier-clients.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams){
    var me = this;
    this.client = {};
    this.carriers = [];
    this.drivers = [];
    //vm.clients = this.clients;
    this.selectedCarrier = '';
    this.selectedDriver = '';
    this.assignTo = '';
    this.assignedDrivers = [];

    this.$onInit = () => {
        this.client = $stateParams.client;
        this.assignTo = this.client.name;
        this.getClients();
    };


    this.getClients = () => {
        fetchScheduledClients($http).then(clients=>{
            this.clients = clients;
        });
        this.fetchCarriers($http).then(carriers=>{
            this.carriers = carriers;
        });
        fetchAssignedDrivers($http, this.client._id).then(assignedDrivers=>{
            this.assignedDrivers = assignedDrivers;
        });
    };


    this.selectCarrier = () => {
        fetchDriversByCarrierId($http, this.selectedCarrier._id)
            .then(drivers=>{
                this.drivers = drivers;
            })
    };

    this.selectDriver = () => {

    };


    this.assign = () => {
        var assignment = {
            carrier: this.selectedCarrier._id,
            driver: this.selectedDriver._id
        };
        if(this.selectedDriver == ''){
            assignment.driver = this.selectedCarrier._id;
        }
        $http.put(`${API_URL}/client/${this.client._id}/assign`, assignment)
            .then(function (response) {
                if(response.data.success){
                    fetchAssignedDrivers($http, me.client._id).then(assignedDrivers=>{
                        console.log(assignedDrivers)
                        me.assignedDrivers = assignedDrivers;
                    });
                }
            })
    };

    this.unAssignDriver = (driver) => {
        $http.put(`${API_URL}/client/${this.client._id}/unassign/${driver._id}`)
            .then(function (response) {
                if(response.data.success){
                    var deletedAssign = me.assignedDrivers.indexOf(driver);
                    me.assignedDrivers.splice(deletedAssign, 1);
                }
            })
    };

    this.fetchCarriers = ($http) => {
        return $http.get(`${API_URL}/carriers`)
            .then(function (response) {
                return response.data.body;
            })
    }
}


function fetchScheduledClients($http){
    return $http.get(`${API_URL}/clients/scheduled`)
        .then(function (response) {
            return response.data.body;
        })
}

function fetchDriversByCarrierId($http, carrierId){
    return $http.get(`${API_URL}/carrier/${carrierId}/drivers`)
        .then(function (response) {
            return response.data.body;
        })
}

function fetchAssignedDrivers($http, clientId){
    return $http.get(`${API_URL}/client/${clientId}/assigned/drivers`)
        .then(function (response) {
            return response.data.body;
        })
}