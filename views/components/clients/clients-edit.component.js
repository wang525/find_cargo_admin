/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("clientsEdit", {
    templateUrl: "./components/clients/clients-edit.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.ccodes = ccodes;
    this.client = $stateParams.client;
    this.client.ccode = this.client.ccode||'45'
    this.error = '';
    this.client_address = this.client.companyDetails ? this.client.companyDetails.companyAddress : '';

    this.countryOptions = [
        {"value": "45", "name": "(+45)"},
        {"value": "46", "name": "(+46)"},
    ];

    new google.maps.places.Autocomplete(document.getElementById('client_address'));

    this.edit = () => {
        if (!this.client.companyDetails) {
            this.client.companyDetails = {};
        }

        if (!this.client_address) {
            this.client.companyDetails.companyAddress = '';
        } else if (this.client_address.formatted_address) {
            this.client.companyDetails.companyAddress = this.client_address.formatted_address;
        } else if (typeof this.client_address == 'string') {
            this.client.companyDetails.companyAddress = this.client_address;
        }

        $http.put(`${API_URL}/client/${this.client._id}`, {client: this.client})
            .then((response) => {
                $state.go('clients');
            }, (response) => {
                this.error = response.data.error;
            })
    }
}