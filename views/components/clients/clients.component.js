/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("clients", {
    templateUrl: "../components/clients/clients.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state) {
    this.clients = [];
    this.filteredClients = [];
    this.search = '';

    this.totalItems = 0;
    this.limit = 50;
    this.currentPage = 1;
    //

    this.$onInit = () => {
        this.getClients();
    };

    this.filterClients = function () {
        this.filteredClients = this.search ? this.clients.filter((item) => {
            let found = false;
            if (item.name) {
                found = item.name.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1
            }

            if (!found && item.email) {
                found = item.email.toLocaleLowerCase().indexOf(this.search.toLocaleLowerCase()) > -1
            }
            return found;
        }) : this.clients.slice(0);
        this.totalItems = this.filteredClients.length;
    };

    this.setPage = function (page) {
        this.currentPage = page;
    };

    this.getClients = () => {
        fetchClients($http).then(clients => {
            console.log("get clients", clients)
            this.clients = clients;
            this.filteredClients = clients.slice();
            this.totalItems = this.filteredClients.length;
        })
    };

    this.activation = (client) => {
        $http.put(`${API_URL}/client/${client._id}/activation`, {approved: client.approved})
        .then(function (response) {
            
        })
    }

    this.editView = (client) => {
        $state.go('clientsEdit', {client: client});
    };

    this.clientsChangePassword = (client) => {
        $state.go('clientsChangePassword', {client: client});
    };

    this.settingsView = (client) => {
        $state.go('settings', {client: client});
    };

    this.accountStatementView = (client) => {
        $state.go('accountStatement', {client: client});
    };

    this.delete = (client) => {
        $http.delete(`${API_URL}/client/${client._id}`)
            .then(response => {
                if (response.data.success) {
                    this.clients.splice(this.clients.indexOf(client), 1);
                    this.filteredClients.splice(this.filteredClients.indexOf(client), 1);
                    this.totalItems = this.filteredClients.length;
                }
            });
    };

    this.schedule = (client) => {
        var isSheduled = '';
        if (client.scheduledClient) {
            isSheduled = '1';
        } else {
            isSheduled = '0';
        }
        $http.put(`${API_URL}/client/${client._id}/schedule`, {schedule: isSheduled})
            .then(function (response) {

            })
    };


    this.activateCarrier = (carrier) => {
        $http.put(`${API_URL}/carrier/${carrier._id}`, {carrier: carrier})
            .then(function (response) {

            })
    };
}


function fetchClients($http) {
    return $http.get(`${API_URL}/clients`)
        .then(function (response) {
            return response.data.body;
        })
}