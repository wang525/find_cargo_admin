/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("scheduledSettings", {
    templateUrl: "../components/clients/scheduled-settings.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", "googlePlacesApi", controller]
});

function controller($http, $state, $stateParams){
    this.client = $stateParams.client;
    this.showSuccess = false;
    this.showMsg = '';
    this.bufAddresses = [];
    this.settings = {
        addresses: [],
        pickup_deadline: '',
        pickup_deadline_to: '',
        delivery_window: '',
        delivery_area: '',
        delivery_window_start: '',
        delivery_window_end: '',
        allow_to_send_email: null,
        email_notification_setting:{
            delivery_created: true,
            delivery_assigned:true,
            delivery_status_changed:true,
            delivery_arrive_before:true,
            delivery_finished:true,
            delivery_delayed:true
        },
        allow_to_send_SMS: null,
        sms_notification_setting:{
            delivery_created: true,
            delivery_assigned:true,
            delivery_status_changed:true,
            delivery_arrive_before:true,
            delivery_finished:true,
            delivery_delayed:true
        },
        allow_to_reschedule: null,
        allow_to_receive_daily_report: null,
        allow_to_receive_end_of_day_report: null,
        allow_to_receive_end_of_week_report: null,
        home_delivery: null,
        store_pickup: null,
        week_days_available: [

        ],
        zip: [

        ],
        delivery_windows: [

        ],
        allowed_pickups_zones: [],
        webshipr_integration_enabled: null,
        dropshipping_enabled: null,
        service_enabled:[],
        allow_delivery:null,
        allow_return:null,
        allow_express:null
    };

    this.enabled_services = [];

    this.allowed_pickups_zone = {
        range_from: '',
        range_to: '',
    }

    this.zip = {
        range_from: '',
        range_to: '',
        price: null
    };

    this.delivery_windows = {
        zip_from: '',
        zip_to: '',
        time_from: '',
        time_to: ''
    };

    this.week_days_available = {
        Monday: {
            checked: false
        },
        Tuesday: {
            checked: false
        },
        Wednesday: {
            checked: false
        },
        Thursday: {
            checked: false
        },
        Friday: {
            checked: false
        },
        Saturday: {
            checked: false
        },
        Sunday: {
            checked: false
        }
        // {checked: false, name: 'Monday'}, {checked: false, name: 'Tuesday'}, {checked: false, name: 'Wednesday'},
        // {checked: false, name: 'Thursday'}, {checked: false, name: 'Friday'}, {checked: false, name: 'Saturday'}, {checked: false, name: 'Sunday'}
    };


    this.$onInit = () => {
        this.fetchServices($http).then(services=>{
            //console.log('enabled_services', services)
            this.enabled_services = services;
            this.getScheduleSettings();
        });
    };

    this.fetchServices = ($http) => {
        return $http.get(`${API_URL}/enabled-services`)
            .then(function (response) {
                return response.data.body;
            })
    };

    this.checkDays = (key) => {
        index = this.settings.week_days_available.indexOf(key);
        if(index == -1)
        {   
            this.settings.week_days_available.push(key);
        }
        else{
            this.settings.week_days_available.splice(index, 1);
        }
        
    };

    this.checkedDays = (days) => {
        for(let day of days){
            this.week_days_available[day].checked = true;
        }
    };

    this.getScheduleSettings = () => {
        console.log("this.cliente ==>", this.client)
        if (this.client != null) {
            fetchSettings($http, this.client._id).then(settings=>{
                if(settings){
                    let  initial_notification_setting = {
                        delivery_created: true,
                        delivery_assigned:true,
                        delivery_status_changed:true,
                        delivery_arrive_before:true,
                        delivery_finished:true,
                        delivery_delayed:true
                    };
                    this.checkedDays(settings.week_days_available);
            
                    if (settings.email_notification_setting==null) {
                        settings.email_notification_setting = JSON.parse(JSON.stringify(initial_notification_setting));
                    }
    
                    if (settings.sms_notification_setting==null) {
                        settings.sms_notification_setting = JSON.parse(JSON.stringify(initial_notification_setting));
                    }
    
                    this.settings = settings;
                    if (settings.addresses) {
                        this.bufAddresses = settings.addresses;
                    }
                    this.enabled_services&&this.enabled_services.map(enabled_service=>{
                        enabled_service.country_settings = {
                            'DK':false,
                            'SWE':false,
                            'FIN':false
                        };
                        let foundSetting = settings&&settings.service_enabled.find(service_setting=>{
                            return service_setting.service_id==enabled_service._id
                        })
                        foundSetting&&foundSetting.countries.map(country=>{
                            enabled_service.country_settings[country]=true;
                        })
                    })
                }
            })
        }
    };

    this.addZip = () => {
        this.settings.zip.push(this.zip);
        console.log(this.settings.zip)
    };

    this.addAllowedPickupZone = () => {
        this.settings.allowed_pickups_zones.push(this.allowed_pickups_zone);
    };

    this.addDeliveryWindows = () => {
        this.settings.delivery_windows.push(this.delivery_windows);
    };

    this.scheduledSettingView = (client) => {
        this.client = client;
        $state.go('scheduledSettings', {client: client})
    };

    this.changeDefault = (num) => {
        this.bufAddresses = this.bufAddresses.map(el => {
            el.default = false;
            return el;
        })
        this.bufAddresses[num].default = true;    
    };

    this.addAddress = () => {
        this.bufAddresses.push({value: '', default: false});
        if (this.bufAddresses.length == 1)
            this.changeDefault(0);
    }

    this.deleteAddress = (num) => {
        if (this.bufAddresses[num].default) {
            this.bufAddresses.splice(num, 1);  
            if (this.bufAddresses[this.bufAddresses.length-1])
                this.bufAddresses[this.bufAddresses.length-1].default = true;
        } else {
            this.bufAddresses.splice(num, 1);  
        }
    }

    this.updateSettings = () => {
        this.settings.addresses = this.bufAddresses.map(el => {
            if (el.value.formatted_address) 
                el.value = el.value.formatted_address;
            return el;
        })

        //console.log(this.enabled_services)

        let service_enabled = [];
        let country_codes = ['DK', 'SWE', 'FIN']
        this.enabled_services&&this.enabled_services.map(enabled_service=>{
            let enabled = {};
            enabled.service_id = enabled_service._id;
            enabled.countries = [];
            if (enabled_service.country_settings) {
                country_codes.map(country_code=>{
                    if (enabled_service.country_settings[country_code]) {
                        enabled.countries.push(country_code);
                    }
                })
            }
            service_enabled.push(enabled);
        })
        this.settings.service_enabled = service_enabled;
        console.log('settings', this.settings)

        if (this.client != null) {
            $http.put(`${API_URL}/client/${this.client._id}/schedule-settings`, {schedule: this.settings})
            .then(response => {
                if(response.data.success){
                    this.showSuccess = true;
                    this.showMsg = response.data.msg;
                    $state.go('scheduledClients')
                }
            })
        }
    };

}


function fetchSettings($http, clientId){
    return $http.get(`${API_URL}/client/${clientId}/schedule-settings`)
        .then(function (response) {
            return response.data.body;
        })
}

function findDay(day){
    return
}