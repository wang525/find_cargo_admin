/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("scheduledClients", {
    templateUrl: "../components/clients/scheduled-clients.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", controller]
});

function controller($http, $state){
    var vm = this;
    this.clients = [];
    this.carriers = [];
    vm.clients = this.clients;


    this.$onInit = () => {
        this.getClients();
    };


    this.getClients = () => {
        fetchScheduledClients($http).then(clients=>{
            console.log("get scheduled clients", clients)
            this.clients = clients;
        });
        this.fetchCarriers($http).then(carriers=>{
            this.carriers = carriers;
        });
    };

    this.scheduledSettingView = (client) => {
        $state.go('scheduledSettings', {client: client})
    };

    this.assignCarrierToClientView = (client) => {
        $state.go('assignCarrierClient', {client: client})
    };



    this.unSchedule = (client) => {
        $http.put(`${API_URL}/client/${client._id}/schedule`, {schedule: '0'})
            .then(function (response) {
                var deletedClient = vm.clients.indexOf(client);
                vm.clients.splice(deletedClient, 1);
            })
    }

    this.fetchCarriers = ($http) => {
        return $http.get(`${API_URL}/carriers`)
            .then(function (response) {
                return response.data.body;
            })
    }
}


function fetchScheduledClients($http){
    return $http.get(`${API_URL}/clients/scheduled`)
        .then(function (response) {
            return response.data.body;
        })
}