/**
 * Created by jeton on 6/20/2017.
 */
var module = angular.module("app");
module.component("clientsChangePassword", {
    templateUrl: "./components/clients/clients-change-password.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.ccodes = ccodes;
    this.client = $stateParams.client;
    console.log(this.client);
    this.error = '';
    this.client.password = '';
    //this.client_address = this.client.companyDetails ? this.client.companyDetails.companyAddress : '';

    //new google.maps.places.Autocomplete(document.getElementById('client_address'));

    this.edit = () => {
        console.log(this.client.password);
        console.log(this.client.confirmPassword);
        $http.put(`${API_URL}/client/change-password/${this.client._id}`, {client: this.client})
            .then((response) => {
                // $state.go('clients');
                history.go(-1)
            }, (response) => {
                this.error = response.data.error;
            })
    }

    this.cancel = () => {
        history.go(-1)
    }
}