var module = angular.module("app");
module.component("clientsCreate", {
    templateUrl: "./components/clients/clients-create.component.html",
    controllerAs: "model",
    controller: ["$http", "$state", "$stateParams", controller]
});

function controller($http, $state, $stateParams) {
    this.client = {};
    this.error = '';

    this.create = () => {
        $http.post(`${API_URL}/clients`, this.client)
            .then((response) => {
                $state.go('clients');
            }, (response) => {
                this.error = response.data.error;
            })
    }

}