/**
 * Created by jeton on 6/20/2017.
 */
(function () {
var module = angular.module("app");
module.component("dashboardMain", {
    templateUrl: "../components/dashboard/dashboard.component.html",
    controller: ["authService", controller],
    controllerAs: "vm"
});


function controller(authService){
    var vm = this;
    vm.auth = authService;
}

})();