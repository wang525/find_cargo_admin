/**
 * Created by jeton on 6/27/2017.
 */
(function () {

    'use strict';

    angular
        .module('app')
        .controller('CallbackController', callbackController);

    function callbackController() {}

})();