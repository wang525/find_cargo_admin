/**
 * Created by jeton on 6/20/2017.
 */

var app = angular.module('app', [
    'auth0.auth0', 
    'ui.router', 
    'google.places', 
    'ngTagsInput', 
    'ui.select', 
    'ngSanitize',  
    // 'toastr', 
    'ui.bootstrap', 
    '720kb.datepicker', 
    'btford.socket-io'
]);


app.config(config);

config.$inject = [
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    'angularAuth0Provider',
];


function config($stateProvider, $urlRouterProvider, $locationProvider, angularAuth0Provider){

    $stateProvider.state('login', {
        url: '/login',
        component: 'login'
    })

    .state('invite', {
        url: '/invite',
        component: 'invite'
    })

    .state('organizations', {
        url: '/organizations',
        component: 'organizations'
    })

    .state('organizationEdit', {
        url: '/organization-edit',
        component: 'organizationEdit',
        params: {organization: null}
    })

    .state('organizationDetails', {
        url: '/organization/:organizationId',
        component: 'organizationDetails'
    })

    .state('coworkerEdit', {
        url: '/coworker-edit',
        component: 'coworkerEdit',
        params: {coworker: null}
    })

    .state('callback', {
        url: '/callback',
        component: 'callback'
    })

    .state('dashboard', {
        url: '/',
        component: 'dashboardMain'
    })

    .state('home', {
        url: '/home',
        component: 'dashboardMain'
    })

    .state('carriers', {
        url: '/carriers',
        component: 'carriers'
    })

    .state('carriersDetails', {
        url: '/carriers-details/:carriersId',
        component: 'carriersDetails'
    })
    
    .state('carriersEdit', {
        url: '/carrier-edit',
        component: 'carriersEdit',
        params : {carrier: null}
    })

    .state('carriersCreate', {
        url: '/carrier-create',
        component: 'carriersCreate'
    })

    .state('liveVehicles', {
        url: '/live-vehicles',
        component: 'liveVehicles'
    })

    .state('clients', {
        url: '/clients',
        component: 'clients'
    })
    .state('zones', {
        url: '/zones',
        component: 'zones'
    })

     .state('scheduledClients', {
        url: '/scheduled-clients',
        component: 'scheduledClients'
    })

    .state('scheduledSettings', {
        url: '/scheduled-settings',
        component: 'scheduledSettings',
        params: {client: null}
    })
    
    .state('services', {
        url: '/services',
        component: 'services'
    })

    .state('serviceEdit', {
        url: '/service/edit',
        component: 'serviceEdit',
        params : {service: null}
    })

    .state('serviceCreate', {
        url: '/service/create',
        component: 'serviceCreate'
    })
    
    .state('pickups', {
        url: '/pickups',
        component: 'plannedPickups'
    })

    .state('insurances', {
        url: '/insurances',
        component: 'insurances'
    })

    .state('smsemail', {
        url: '/smsemail',
        component: 'smsemail'
    })

    .state('settings', {
        url: '/settings',
        component: 'apiKeys',
        params : {client: null}
    })

    .state('accountStatement', {
        url: '/accountStatement',
        component: 'accountStatement',
        params : {client: null}
    })

    .state('assignCarrierClient', {
        url: '/assign-carrier-client',
        component: 'assignCarrierClient',
        params: {client: null}
    })

    .state('assignDriverZone', {
        url: '/assign-driver-zone',
        component: 'assignDriverZone',
    })

    .state('assignDriverDepartment', {
        url: '/assign-driver-department',
        component: 'assignDriverDepartment',
    })

    .state('routePlanning', {
        url: '/route-planning',
        component: 'routePlanning'
    })

    .state('warehouseOverview', {
        url: '/warehouse-overview',
        component: 'warehouseOverview'
    })
    .state('areaRouting', {
        url: '/area-routing',
        component: 'areaRouting'
    })

    .state('zonesEdit', {
        url: '/zone/edit',
        component: 'zonesEdit',
        params : {zone: null}
    })
    .state('zonesCreate', {
        url: '/zone/create',
        component: 'zonesCreate',
    })
    .state('postCodes', {
        url: '/zone/postCodes',
        component: 'postCodes',
        params : {zone: null, isNew: null}
    })
    .state('postCodesCreate', {
        url: '/zone/postCodesCreate',
        component: 'postCodesCreate',
        params : {zone: null}
    })
    .state('postCodesEdit', {
        url: '/zone/postCodesEdit',
        component: 'postCodesEdit',
        params : {postCode: null}
    })
    .state('clientsEdit', {
        url: '/client/edit',
        component: 'clientsEdit',
        params : {client: null}
    })
    .state('clientsChangePassword', {
        url: '/client/changePassword',
        component: 'clientsChangePassword',
        params : {client: null}
    })
    .state('clientsCreate', {
        url: '/client/create',
        component: 'clientsCreate'
    })

    .state('assignDriverDelivery', {
        url: '/assign-driver-delivery',
        component: 'assignDriverDelivery'
    })

    .state('assignDriverEditDelivery', { 
        url: '/assign-driver-delivery/:deliveryId',
        component: 'assignDriverEditDelivery'
    })

    .state('adminActions', {
        url: '/adminActions',
        component: 'adminActions'
    });


    angularAuth0Provider.init({
        clientID: 'Ra3bewe3Fxmr3PiJPSamoY3gxJ4hiei4',
        domain: 'jeton.eu.auth0.com',
        responseType: 'token id_token',
        audience: 'https://jeton.eu.auth0.com/userinfo',
        redirectUri: 'http://localhost:9080/#!/callback',
        scope: 'openid'
    });


    //
    // $urlRouterProvider.otherwise('/s');
    //
    // $locationProvider.hashPrefix('');
    //
    // $locationProvider.html5Mode(true);

}