/**
 * Created by jeton on 6/14/2017.
 */
var passport = require('passport');
var Auth0Strategy = require('passport-auth0');
var AuthConfig = require('../config/auth0');

module.exports = (function () {

//This will configure Passport to use Auth0
    var strategy = new Auth0Strategy(AuthConfig.strategy, function(accessToken, refreshToken, extraParams, profile, done) {
        // accessToken is the token to call Auth0 API (not needed in the most cases)
        // extraParams.id_token has the JSON Web Token
        // profile has all the information from the user
        return done(null, profile);
    });

    passport.use(strategy);

// you can use this section to keep a smaller payload
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
});