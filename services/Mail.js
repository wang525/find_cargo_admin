/**
 * Created by jeton on 6/29/2017.
 */
const api_key = 'key-52e3854a2fd0ebefea915d71ee3080d3';
const domain = 'nemlevering.dk';
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

let options = {
    from: 'Nemlevering <accounts@nemlevering.com>',
    to: '',
    subject: '',
    text: ''
};

let Mail = {
    send: () => {
        mailgun.messages().send(options, function (error, body) {
            console.log(body);
        });
    },
    setOptions: (sendTo, subject, html, callback) => {
        options.to = sendTo;
        options.subject = subject;
        options.html = html;
        callback();
    },
    createCarrierMsg: (email) => {
        return `Hi ` + email + `,<br><br>
Welcome to IDAG.<br><br>
We are here to help you make more money with leads and free tools.<br><br>
Please start by downloading our app at https://goidag.com/download.<br><br>
Best wishes,<br>
- IDAG team`
    },
};

module.exports = Mail;