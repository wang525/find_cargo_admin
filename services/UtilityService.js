const Crypto = require('crypto');

let UtilityService = {
    randomCrypto: randomCrypto,
};

function randomCrypto(length = 16) {
    return new Promise((resolve, reject) => {
        Crypto.randomBytes(length, (err, buffer) => {
            if (err) {
                reject(err);
            } else {
                resolve(buffer.toString('hex'));
            }
        });
    });
}
module.exports = UtilityService;
