var express = require('express'),
    exphbs = require('express-handlebars'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    exphbs = require('express-handlebars'),
    session = require('express-session');
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
var logout = require('express-passport-logout');

var AuthController = require('./controllers/AuthController');
var MainController = require('./controllers/MainController');

var Carrier = require('./routes/carrier');
var Clients = require('./routes/clients');
var Organization = require('./routes/organization');
var Zones = require('./routes/zones');
var Deliveries = require('./routes/deliveries');
var Assign = require('./routes/assign');
var Reports = require('./routes/reports');
var Services = require('./routes/services');

var app = express();
var ConnectMongo = require('./utils/ConnectMongo');
var passport = require('passport');
var AuthService = require('./services/AuthService')();
var AdminActionsLog = require('./middlewares/AdminActionsLog')
var unless = require('./middlewares/Unless');


// Express configuration
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(session({
    secret: 'y3i72g4b634',
    resave: true,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static('views'));
app.use(unless(['css', 'js', 'html'], AdminActionsLog.log));

// Configure express to use handlebars templates
var hbs = exphbs.create();
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

var port = process.env.PORT || 9080; // select your port or let it pull from your .env file

app.get('/',  ensureLoggedIn, MainController.dashboard);
// app.get('/dashboard',  ensureLoggedIn, MainController.dashboard);
app.get('/login', AuthController.login);

app.get('/logout', function (req, res) {
    req.logout();
    req.user = null;
    res.redirect('/login');
});

app.use('/api', Carrier);
app.use('/api', Clients);
app.use('/api', Organization);
app.use('/api', Zones);
app.use('/api', Deliveries);
app.use('/api', Assign);
app.use('/api', Reports);
app.use('/api', Services);


app.get('/callback',
    passport.authenticate('auth0', { failureRedirect: '/login' }),
    function(req, res) {
        res.redirect(req.session.returnTo || '/');
    });

app.listen(port, function() {
	console.log('App listening on port ' + port);
});