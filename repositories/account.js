const mongoose = require("mongoose");
const q = require("q");
mongoose.Promise = require("bluebird");
const Account = mongoose.models.accounts;

let Repository = {
    getById: getById,
    getByEmail: getByEmail,
    createAccount: createAccount,
    updateAccount: updateAccount
};

function updateAccount(account) {
    return account.save();
}

function createAccount(userData) {
    let deferred = q.defer();
    let accountModel = new Account(userData);
    accountModel.save(function (err, data) {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(accountModel);
        }
    });

    return deferred.promise;
}

function getById(id) {
    let deferred = q.defer();

    Account.findById(id, function (err, userAccount) {
        if (err) {
            return deferred.reject(err);
        }

        deferred.resolve(userAccount);
    });

    return deferred.promise;
}

function getByEmail(email) {
    let deferred = q.defer();
    let query = {
        email: email.toLowerCase(),
    };

    Account.findOne(query, function (err, account) {
        if (err) {
            return deferred.reject(err);
        }
        deferred.resolve(account);
    });

    return deferred.promise;
}

module.exports = Repository;
