const mongoose = require("mongoose");
const q = require("q");
mongoose.Promise = require("bluebird");
require('../models/Departments');
const Department = mongoose.models.departments;

let Repository = {
    createDepartment: createDepartment,
    generateDefaultDepartment: generateDefaultDepartment,
};

function createDepartment(data) {
    let deferred = q.defer();
    let model = new Department(data);
    model.save((err, data) => {
        if (err) {
            deferred.reject(err);
        }
        else {
            deferred.resolve(model);
        }
    });

    return deferred.promise;
}

function generateDefaultDepartment(companyId) {
    return {
        companyId: companyId,
        companyName: null,
        departments: [
            {
                default: true,
                typeOfDelivery: 'Distribution',
                name: 'Default',
                editorEnabled: false
            }
        ]
    };
}

module.exports = Repository;
