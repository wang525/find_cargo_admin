let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let AdminActionsLogSchema = new Schema({
    date: Date,
    method: String,
    url: String,
    description: String
});

let adminactionslogs = mongoose.model('adminactionslogs', AdminActionsLogSchema);
module.exports = adminactionslogs;