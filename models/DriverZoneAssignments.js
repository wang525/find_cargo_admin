/**
 * Created by jeton on 8/25/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DriverZoneAssignmentsSchema = new Schema({
    driver: {
        type: Schema.ObjectId,
        ref: 'accounts'
    },
    zone: String
});

module.exports = mongoose.model('DriverZoneAssignments', DriverZoneAssignmentsSchema);