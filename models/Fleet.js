var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Fleet = new Schema({
    "identifier": {
        "type": String,
        "required": true
    },
    "vehicalType": {
        "type": String,
        "required": true
    },
    "type": {
        "type": String,
        "required": true
    },
    "allowCargo": {
        "type": String,
        "required": true
    },
    "userID": {
        "type": ObjectId,
        "required": true
    },
    "driver": {
        "type": String
    }
}, { collection: 'truck' });

module.exports = mongoose.model("truck", Fleet);


