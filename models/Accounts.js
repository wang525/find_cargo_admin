/**
 * Created by jeton on 6/20/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AccountSchema = new Schema({
    id: {
        type: String
    },
    name: String,
    email: {
        type: String,
        index: true,
        unique: true
    },
    ccode: String,
    codeISO: String,
    phone: String,
    category: String,
    buyer: String,
    scheduledClient: String,
    freightForwarder: Boolean,
    carrier: String,
    manager : Boolean,
    approved : Boolean,
    monthlyInvoice: Number,
    activeCarrier: String,
    active: {
        type: Number,
        default: 1
    },
    date: {type: Date, default: Date.now()},
    groupId: String,
    company: Schema.Types.ObjectId,
    cpr: String,
    companyDetails: {
        companyName: String,
        taxId: String,
        companyAddress: String
    },
    scheduledSettings: {
       address: String,
       pickupDeadline: String,
       deliveryWindowStart: String,
       deliveryWindowEnd: String,
       allowToSendEmail: Boolean,
       allowToSendSMS: Boolean
    },
    scheduledCarriers: Array,
    isAdmin: Boolean,
    apikey: String,
    supportEmail: String,
    supportPhone: String,
    password: String,
    insurance: Boolean,
});


var accounts = mongoose.model('accounts', AccountSchema);
module.exports = accounts;
