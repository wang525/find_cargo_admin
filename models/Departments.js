/**
 * Created by jeton on 8/25/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DepartmentsSchema = new Schema({
    companyId: Schema.Types.ObjectId,
    companyName: String,
    departments: [
        {
            editorEnabled: Boolean,
            name: String,
            default: Boolean,
            typeOfDelivery: String,
        }

    ]
});

var departments = mongoose.model('departments', DepartmentsSchema);
module.exports = departments;