/**
 * Created by jeton on 8/25/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ZonesSchema = new Schema({
    "COUNTRY CODE": {
        type: String,
    },
    "AREA": {
        type: String
    },
    "ZONE": {
        type: Number
    },
    "ZIP": {
        type: Number
    },
    "CITY": {
        type: String
    },
    "STREET": {
        type: String
    },
    "Land": {
        type: String
    },
    "EVENING": {
        type: Boolean
    },
    "SAMEDAY": {
        type: Boolean
    },
});


var accounts = mongoose.model('post_codes', ZonesSchema);
module.exports = accounts;