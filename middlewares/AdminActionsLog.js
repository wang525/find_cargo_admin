let AdminActionsLogModel = require('../models/AdminActionsLog');

let RequestLog = {
    adminActionDescriptions: {
        "^GET/api\/reports\/adminActions": "Request admin actions list",
        "^GET/$": "Open Home page",
        "^GET/login$": "Login",
        "^GET/callback": "Auth callback",
        "^GET/api/organizations$": "Get organizations",
        "^GET/api/organization": "View organization",
        "^DELETE/api/organization": "Delete organization",
        "^PUT/api/organization": "Edit organization",
        "^GET/api/clients$": "Get clients",
        "^PUT/api/client/.*/schedule$": "Edit client",
        "^PUT/api/client/.*/activation$": "Activate client",
        "^POST/api/clients$": "Create client",
        "^GET/api/clients/.*/payments$": "Get payments",
        "^GET/api/clients/.*/expenses$": "Get expenses",
        "^POST/api/clients/.*/expenses$": "Create expenses",
        "^POST/api/clients/.*/payments$": "Create payment",
        "^PUT/api/clients/.*/payments": "Edit payment",
        "^DELETE/api/clients/.*/payments": "Delete payment",
        "^PUT/api/clients/.*/expenses": "Edit expense",
        "^DELETE/api/clients/.*/expenses": "Delete expense",
        "^PUT/api/client/.*/insurance$": "Add insurance",
        "^PUT/api/client/.*$": "Edit client",
        "^DELETE/api/client": "Delete client",
        "^GET/api/carriers$": "Get carriers",
        "^GET/api/clients/scheduled$": "Get scheduled clients",
        "^GET/api/client/.*/assigned/drivers$": "Get assigned drivers",
        "^GET/api/carrier/.*/drivers$": "Get drivers",
        "^PUT/api/client/.*/assign$": "Assign driver",
        "^PUT/api/client/.*/unassign/.*$": "Unassign driver",
        "^GET/api/client/.*/schedule-settings$": "Get scheduled client settings",
        "^PUT/api/client/.*/schedule-settings$": "Edit scheduled client settings",
        "^PUT/api/client/.*/schedule$": "Edit scheduled client",
        "^GET/api/insurances/.*/.*$": "Get insurances",
        "^GET/api/zones$": "Get zones",
        "^DELETE/api/zone/postCodes/.*$": "Delete post code",
        "^GET/api/zone/postCodes": "Get post codes",
        "^POST/api/zone/postCodes$": "Delete post code",
        "^GET/api/zones$": "Get zones",
        "^PUT/api/zone$": "Edit zone",
        "^DELETE/api/zones": "Delete zone",
        "^POST/api/zone/postCodes$": "Create post code",
        "^GET/api/carriers": "Get carriers",
        "^DELETE/api/un-assign/.*/driver-department$": "Delete driver department",
        "^GET/api/assigned/departments$": "Get driver departments",
        "^POST/api/assign/driver-department$": "Create driver department",
        "^GET/api/departments$": "Get departments",
        "^DELETE/api/un-assign/.*$": "Delete assignment",
        "^GET/api/assigned/driver-zone$": "Get driver zones",
        "^POST/api/assign/driver-zone$": "Create driver zone",
        "^GET/api/zones$": "Get zones",
        "^GET/api/deliveries$": "Get deliveries",
        "^DELETE/api/carrier/.*$": "Delete carrier",
        "^PUT/api/carrier/.*$": "Edit carrier",
        "^POST/api/carriers$": "Create carrier"        
    },

    log: function (req, res, next) {
        if (!['GET', 'POST', 'PUT', 'DELETE'].includes(req.method)) {
            return next();
        }

        try {
            let log = new AdminActionsLogModel({
                date: new Date(),
                method: req.method,
                url: req.url,
                description: RequestLog.getUrlDescription(req.url, req.method)
            });

            AdminActionsLogModel.create(log);
        } catch (err) {
            // nothing
        }

        next();
    },

    getUrlDescription: function (url, method) {
        for (var pattern in this.adminActionDescriptions) {
            if (new RegExp(pattern).test(method + url)) {
                return RequestLog.adminActionDescriptions[pattern];
            }
        }

        return '';
    }
};

module.exports = RequestLog;