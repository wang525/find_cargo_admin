let unless = function (paths, middleware) {
    return function (req, res, next) {
        if (paths.find(x => { return req.url.includes(x); })) {
            return next();
        } else {
            return middleware(req, res, next);
        }
    };
};

module.exports = unless;